/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { Producer } from "../index";
import { ByteOutput } from "bitmanipulation-ts";

export class AddressProducer<ByteOutputT extends ByteOutput> implements Producer<ByteOutputT> {
  private readonly address: Buffer;

  constructor(address: Buffer) {
    if (address.length !== 21) {
      throw new Error(
        `Address must have length 21 bytes, got length = ${
          address.length
        }, value = ${address.toString("hex")}`
      );
    }
    this.address = address;
  }

  public write(out: ByteOutputT): void {
    out.writeBytes(this.address);
  }
}
