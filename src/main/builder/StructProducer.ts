/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { ContractAbi } from "../parser/ContractAbi";
import { NamedTypeRef, StructTypeSpec, TypeSpec } from "../types/Abi";
import { Producer } from "../types/Producer";
import { AbstractBuilder } from "./AbstractBuilder";
import { ByteOutput } from "bitmanipulation-ts";

export class StructProducer<ByteOutputT extends ByteOutput>
  extends AbstractBuilder<ByteOutputT>
  implements Producer<ByteOutputT>
{
  private readonly structTypeSpec: StructTypeSpec | null;
  private readonly elements: Array<Producer<ByteOutputT>> = [];

  constructor(
    contractAbi: ContractAbi | null,
    errorPath: string,
    structTypeRef?: NamedTypeRef | null
  ) {
    super(contractAbi, errorPath);

    if (structTypeRef === null) {
      this.structTypeSpec = null;
    } else {
      let structRef: NamedTypeRef;
      if (contractAbi === null) {
        throw Error("Null pointer exception");
      }

      if (structTypeRef === undefined) {
        structRef = contractAbi.stateType as NamedTypeRef;
      } else {
        structRef = structTypeRef;
      }
      const namedType = contractAbi.namedTypes[structRef.index];
      this.structTypeSpec = namedType as StructTypeSpec;
    }
  }

  public override write(out: ByteOutputT) {
    this.validate();
    for (const element of this.elements) {
      element.write(out);
    }
  }

  public override addElement(argument: Producer<ByteOutputT>) {
    this.elements.push(argument);
  }

  public override getTypeSpecForArgument(): TypeSpec | null {
    if (this.structTypeSpec !== null) {
      const lastIndex = this.elements.length;
      if (lastIndex >= this.structTypeSpec.fields.length) {
        throw new Error("Cannot add more arguments than the struct has fields.");
      }
      return this.structTypeSpec.fields[lastIndex].type;
    }
    return null;
  }

  private validate() {
    if (
      this.structTypeSpec !== null &&
      this.elements.length !== this.structTypeSpec.fields.length
    ) {
      const missingArgument = this.structTypeSpec.fields[this.elements.length].name;
      throw new Error(`Missing argument '${missingArgument}'`);
    }
  }

  override getFieldName(): string {
    if (this.structTypeSpec !== null) {
      const lastIndex = this.elements.length;
      return "/" + this.structTypeSpec.fields[lastIndex].name;
    }
    return "";
  }
}
