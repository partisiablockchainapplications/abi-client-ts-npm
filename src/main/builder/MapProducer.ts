/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { ContractAbi } from "../parser/ContractAbi";
import { MapTypeSpec, TypeSpec } from "../types/Abi";
import { Producer } from "../types/Producer";
import { AbstractBuilder } from "./AbstractBuilder";
import { ByteOutput } from "bitmanipulation-ts";

export class MapProducer<ByteOutputT extends ByteOutput>
  extends AbstractBuilder<ByteOutputT>
  implements Producer<ByteOutputT>
{
  private readonly mapTypeSpec: MapTypeSpec | null;
  private readonly elements: Array<Producer<ByteOutputT>> = [];

  constructor(mapTypeSpec: MapTypeSpec | null, contractAbi: ContractAbi | null, errorPath: string) {
    super(contractAbi, errorPath);
    this.mapTypeSpec = mapTypeSpec;
  }

  public override write(out: ByteOutputT) {
    if (this.elements.length % 2 === 1) {
      throw new Error("Missing value for key");
    }
    out.writeI32(this.size());
    for (const element of this.elements) {
      element.write(out);
    }
  }

  public override addElement(argument: Producer<ByteOutputT>) {
    this.elements.push(argument);
  }

  public override getTypeSpecForArgument(): TypeSpec | null {
    if (this.mapTypeSpec !== null) {
      if (this.elements.length % 2 == 0) {
        return this.mapTypeSpec.keyType;
      } else {
        return this.mapTypeSpec.valueType;
      }
    }
    return null;
  }

  public size(): number {
    return Math.trunc((this.elements.length + 1) / 2);
  }

  getFieldName(): string {
    const index = Math.trunc(this.elements.length / 2);
    if (this.elements.length % 2 == 0) {
      return "/" + index + "/key";
    } else {
      return "/" + index + "/value";
    }
  }
}
