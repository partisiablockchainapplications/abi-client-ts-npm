/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { ByteOutput } from "bitmanipulation-ts";
import { ContractAbi } from "../parser/ContractAbi";
import { EnumTypeSpec, NamedTypeRef, TypeSpec } from "../types/Abi";
import { Producer } from "../types/Producer";
import { AbstractBuilder } from "./AbstractBuilder";
import { StructProducer } from "./StructProducer";

export class EnumVariantProducer<ByteOutputT extends ByteOutput>
  extends AbstractBuilder<ByteOutputT>
  implements Producer<ByteOutputT>
{
  private readonly structProducer: StructProducer<ByteOutputT>;
  private readonly discriminant: number;
  private readonly variantName: string;

  constructor(
    contractAbi: ContractAbi | null,
    errorPath: string,
    enumTypeRef: NamedTypeRef | null,
    discriminant: number
  ) {
    super(contractAbi, errorPath);

    const variantRef = EnumVariantProducer.getStructTypeRef(
      contractAbi,
      errorPath,
      enumTypeRef,
      discriminant
    );
    this.variantName = EnumVariantProducer.getVariantName(contractAbi, variantRef);

    this.structProducer = new StructProducer<ByteOutputT>(contractAbi, errorPath, variantRef);
    this.discriminant = discriminant;
  }

  private static getStructTypeRef(
    contractAbi: ContractAbi | null,
    errorPath: string,
    enumTypeRef: NamedTypeRef | null,
    discriminant: number
  ): NamedTypeRef | null {
    if (contractAbi == null || enumTypeRef == null) {
      return null;
    } else {
      const enumTypeSpec = contractAbi.getNamedType(enumTypeRef) as EnumTypeSpec;
      const variant = enumTypeSpec.variant(discriminant);
      if (variant === null) {
        throw new Error(
          `In ${errorPath}, Undefined variant discriminant ${discriminant} for ${enumTypeSpec.name}`
        );
      } else {
        return variant.def;
      }
    }
  }

  private static getVariantName(
    contractAbi: ContractAbi | null,
    variantRef: NamedTypeRef | null
  ): string {
    if (contractAbi == null || variantRef == null) {
      return "";
    } else {
      return contractAbi.getNamedType(variantRef).name;
    }
  }

  public override write(out: ByteOutputT) {
    out.writeU8(this.discriminant);
    this.structProducer.write(out);
  }

  public override addElement(argument: Producer<ByteOutputT>) {
    this.structProducer.addElement(argument);
  }

  public override getTypeSpecForArgument(): TypeSpec | null {
    return this.structProducer.getTypeSpecForArgument();
  }

  public override getFieldName(): string {
    return "/" + this.variantName + this.structProducer.getFieldName();
  }
}
