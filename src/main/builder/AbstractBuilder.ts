/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import BN from "bn.js";
import {
  AddressProducer,
  EnumVariantProducer,
  MapProducer,
  OptionBuilder,
  Producer,
  StructProducer,
  TypeIndex,
  VecProducer,
} from "../index";
import { ContractAbi } from "../parser/ContractAbi";
import {
  MapTypeSpec,
  NamedTypeRef,
  OptionTypeSpec,
  SizedByteArrayTypeSpec,
  TypeSpec,
  VecTypeSpec,
} from "../types/Abi";
import * as Buffer from "buffer";
import { ByteOutput } from "bitmanipulation-ts";

export abstract class AbstractBuilder<ByteOutputT extends ByteOutput>
  implements Producer<ByteOutputT>
{
  protected readonly contractAbi: ContractAbi | null;
  private readonly errorPath: string;

  protected constructor(contractAbi: ContractAbi | null, errorPath: string) {
    this.contractAbi = contractAbi;
    this.errorPath = errorPath;
  }

  public addBool(value: boolean): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg({ write: (out) => out.writeBoolean(value) }, TypeIndex.bool);
    return this;
  }

  public addString(value: string): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg({ write: (out) => out.writeString(value) }, TypeIndex.String);
    return this;
  }

  public addAddress(value: Buffer): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg(new AddressProducer(value), TypeIndex.Address);
    return this;
  }

  public addU8(value: number): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg({ write: (out) => out.writeU8(value) }, TypeIndex.u8);
    return this;
  }

  public addU16(value: number): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg({ write: (out) => out.writeU16(value) }, TypeIndex.u16);
    return this;
  }

  public addU32(value: number): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg({ write: (out) => out.writeU32(value) }, TypeIndex.u32);
    return this;
  }

  public addU64(value: BN | number): AbstractBuilder<ByteOutputT> {
    let value2: BN;
    if (typeof value === "number") {
      value2 = new BN(value);
    } else {
      value2 = value;
    }
    this.typeCheckAndAddArg({ write: (out) => out.writeU64(value2) }, TypeIndex.u64);
    return this;
  }

  public addU128(value: BN): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg(
      { write: (out) => out.writeUnsignedBigInteger(value, 16) },
      TypeIndex.u128
    );
    return this;
  }

  public addI8(value: number): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg({ write: (out) => out.writeI8(value) }, TypeIndex.i8);
    return this;
  }

  public addI16(value: number): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg({ write: (out) => out.writeI16(value) }, TypeIndex.i16);
    return this;
  }

  public addI32(value: number): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg({ write: (out) => out.writeI32(value) }, TypeIndex.i32);
    return this;
  }

  public addI64(value: BN | number): AbstractBuilder<ByteOutputT> {
    let value2: BN;
    if (typeof value === "number") {
      value2 = new BN(value);
    } else {
      value2 = value;
    }
    this.typeCheckAndAddArg({ write: (out) => out.writeI64(value2) }, TypeIndex.i64);
    return this;
  }

  public addI128(value: BN): AbstractBuilder<ByteOutputT> {
    this.typeCheckAndAddArg(
      { write: (out) => out.writeSignedBigInteger(value, 16) },
      TypeIndex.i128
    );
    return this;
  }

  public addStruct(): StructProducer<ByteOutputT> {
    const struct = this.getTypeSpecForArgument();
    this.typeCheck(TypeIndex.Named);

    const structProducer = new StructProducer<ByteOutputT>(
      this.contractAbi,
      this.errorPath + this.getFieldName(),
      struct as NamedTypeRef | null
    );
    this.addElement(structProducer);
    return structProducer;
  }

  public addEnumVariant(discriminant: number): EnumVariantProducer<ByteOutputT> {
    const enumTypeRef = this.getTypeSpecForArgument();
    this.typeCheck(TypeIndex.Named);

    const enumVariantProducer = new EnumVariantProducer<ByteOutputT>(
      this.contractAbi,
      this.errorPath + this.getFieldName(),
      enumTypeRef as NamedTypeRef | null,
      discriminant
    );
    this.addElement(enumVariantProducer);
    return enumVariantProducer;
  }

  public addVec(): VecProducer<ByteOutputT> {
    const vec = this.getTypeSpecForArgument();
    this.typeCheck(TypeIndex.Vec);

    const vecProducer = new VecProducer<ByteOutputT>(
      vec as VecTypeSpec | null,
      this.contractAbi,
      this.errorPath + this.getFieldName()
    );
    this.addElement(vecProducer);
    return vecProducer;
  }

  public addVecU8(values: Buffer): AbstractBuilder<ByteOutputT> {
    this.typeCheck(TypeIndex.Vec);
    const vecType = this.getTypeSpecForArgument() as VecTypeSpec | null;

    if (vecType !== null) {
      const expectedType = vecType.valueType.typeIndex;
      if (expectedType !== TypeIndex.u8) {
        throw new Error(
          `In ${this.errorPath + this.getFieldName()}, Expected type Vec<${
            TypeIndex[expectedType]
          }>, but got Vec<u8>`
        );
      }
    }
    this.addElement({
      write: (out) => {
        out.writeI32(values.length);
        out.writeBytes(values);
      },
    });
    return this;
  }

  public addMap(): MapProducer<ByteOutputT> {
    const map = this.getTypeSpecForArgument();
    this.typeCheck(TypeIndex.Map);

    const mapProducer = new MapProducer<ByteOutputT>(
      map as MapTypeSpec,
      this.contractAbi,
      this.errorPath + this.getFieldName()
    );
    this.addElement(mapProducer);
    return mapProducer;
  }

  public addSet(): VecProducer<ByteOutputT> {
    return this.addVec();
  }

  public addOption(): OptionBuilder<ByteOutputT> {
    const optionType = this.getTypeSpecForArgument();
    this.typeCheck(TypeIndex.Option);

    const optionBuilder = new OptionBuilder<ByteOutputT>(
      optionType as OptionTypeSpec | null,
      this.contractAbi,
      this.errorPath + this.getFieldName()
    );
    this.addElement(optionBuilder);
    return optionBuilder;
  }

  public addSizedByteArray(values: Buffer): AbstractBuilder<ByteOutputT> {
    const arrayType = this.getTypeSpecForArgument();
    this.typeCheck(TypeIndex.SizedByteArray);

    if (arrayType !== null) {
      const sizedArrayTypeSpec = arrayType as SizedByteArrayTypeSpec;
      const arraySize = sizedArrayTypeSpec.length;
      if (values.length !== arraySize) {
        throw new Error(`Expected ${arraySize} arguments, but got ${values.length}`);
      }
    }
    this.addElement({ write: (out) => out.writeBytes(values) });
    return this;
  }

  abstract addElement(argument: Producer<ByteOutputT>): void;

  abstract getTypeSpecForArgument(): TypeSpec | null;

  abstract write(out: ByteOutputT): void;

  abstract getFieldName(): string;

  typeCheckAndAddArg(argument: Producer<ByteOutputT>, expected: TypeIndex): void {
    this.typeCheck(expected);
    this.addElement(argument);
  }

  typeCheck(actualType: TypeIndex): void {
    const typeSpecForArgument = this.getTypeSpecForArgument();
    if (typeSpecForArgument != null) {
      const expectedType = typeSpecForArgument.typeIndex;
      if (expectedType !== actualType) {
        throw new Error(
          `In ${this.errorPath + this.getFieldName()}, Expected type ${
            TypeIndex[expectedType]
          }, but got ${TypeIndex[actualType]}`
        );
      }
    }
  }
}
