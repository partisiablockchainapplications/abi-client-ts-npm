/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { ContractAbi } from "../parser/ContractAbi";
import { TypeSpec, VecTypeSpec } from "../types/Abi";
import { Producer } from "../types/Producer";
import { AbstractBuilder } from "./AbstractBuilder";
import { ByteOutput } from "bitmanipulation-ts";

export class VecProducer<ByteOutputT extends ByteOutput> extends AbstractBuilder<ByteOutputT> {
  private readonly vec: VecTypeSpec | null;
  private readonly elements: Array<Producer<ByteOutputT>> = [];

  constructor(vec: VecTypeSpec | null, contractAbi: ContractAbi | null, errorPath: string) {
    super(contractAbi, errorPath);
    this.vec = vec;
  }

  public override write(out: ByteOutputT) {
    out.writeI32(this.elements.length);
    for (const element of this.elements) {
      element.write(out);
    }
  }

  public override addElement(argument: Producer<ByteOutputT>) {
    this.elements.push(argument);
  }

  public override getTypeSpecForArgument(): TypeSpec | null {
    if (this.vec !== null) {
      return this.vec.valueType;
    }
    return null;
  }

  public size(): number {
    return this.elements.length;
  }

  public get(index: number): Producer<ByteOutputT> {
    return this.elements[index];
  }

  getFieldName(): string {
    return "/" + this.elements.length;
  }
}
