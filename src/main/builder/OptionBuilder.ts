/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { ContractAbi } from "../parser/ContractAbi";
import { OptionTypeSpec, TypeSpec } from "../types/Abi";
import { Producer } from "../types/Producer";
import { AbstractBuilder } from "./AbstractBuilder";
import { ByteOutput } from "bitmanipulation-ts";

export class OptionBuilder<ByteOutputT extends ByteOutput>
  extends AbstractBuilder<ByteOutputT>
  implements Producer<ByteOutputT>
{
  private readonly optionType: OptionTypeSpec | null;
  private element: Producer<ByteOutputT> | null = null;

  public constructor(
    optionType: OptionTypeSpec | null,
    contractAbi: ContractAbi | null,
    errorPath: string
  ) {
    super(contractAbi, errorPath);
    this.optionType = optionType;
  }

  public override write(out: ByteOutputT) {
    if (this.element !== null) {
      out.writeU8(1);
      this.element.write(out);
    } else {
      out.writeU8(0);
    }
  }

  public override addElement(argument: Producer<ByteOutputT>) {
    if (this.element !== null) {
      throw new Error("Cannot set option value twice.");
    }
    this.element = argument;
  }

  public override getTypeSpecForArgument(): TypeSpec | null {
    if (this.optionType !== null) {
      return this.optionType.valueType;
    }
    return null;
  }

  override getFieldName(): string {
    return "";
  }
}
