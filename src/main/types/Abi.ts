/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { HashMap } from "../HashMap";
import { ContractAbi } from "../parser/ContractAbi";
import { Configuration } from "../parser/Configuration";

export interface FileAbi {
  header: string;
  versionBinder: AbiVersion;
  versionClient: AbiVersion;
  format: Configuration;
  contract: ContractAbi;
}

export interface AbiVersion {
  major: number;
  minor: number;
  patch: number;
}

export interface Header {
  header: string;
  versionBinder: AbiVersion;
  versionClient: AbiVersion;
}

export type FieldAbi = GenericTypeAbi<TypeSpec>;

export interface FnAbi {
  kind: FnKind;
  name: string;
  shortname: Buffer;
  arguments: ArgumentAbi[];
}

export interface FnKind {
  kindId: number;
  minAllowedPerContract: number;
  maxAllowedPerContract: number;
}

export type ArgumentAbi = GenericTypeAbi<TypeSpec>;

export interface GenericTypeAbi<T extends TypeSpec> {
  name: string;
  type: T;
}

export type TypeSpec = SimpleTypeSpec | CompositeTypeSpec | NamedTypeRef;

export type CompositeTypeSpec =
  | VecTypeSpec
  | MapTypeSpec
  | SetTypeSpec
  | SizedByteArrayTypeSpec
  | OptionTypeSpec;

export interface NamedTypeRef {
  typeIndex: TypeIndex.Named;
  index: number;
}

export class NamedTypeSpec {
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}

export class StructTypeSpec extends NamedTypeSpec {
  fields: FieldAbi[];

  constructor(name: string, fields: FieldAbi[]) {
    super(name);
    this.fields = fields;
  }
}

export class EnumTypeSpec extends NamedTypeSpec {
  variants: EnumVariant[];

  constructor(name: string, variants: EnumVariant[]) {
    super(name);
    this.variants = variants;
  }

  public variant(discriminant: number): EnumVariant | null {
    return this.variants.find((variant) => variant.discriminant === discriminant) ?? null;
  }
}

export interface EnumVariant {
  discriminant: number;
  def: NamedTypeRef;
}

export enum ShortnameType {
  leb128,
  hash,
}

export enum FunctionFormat {
  InitSeparately,
  FnKind,
}

export enum NamedTypesFormat {
  OnlyStructs = 1,
  StructsAndEnum = 2,
}

export interface ConfigOption {
  shortnameType: ShortnameType;
  fnType: FunctionFormat;
  namedTypesFormat: NamedTypesFormat;
}

export enum NamedTypeIndex {
  Struct = 0x01,
  Enum = 0x02,
}

export enum TypeIndex {
  Named = 0,
  u8 = 1,
  u16 = 2,
  u32 = 3,
  u64 = 4,
  u128 = 5,
  i8 = 6,
  i16 = 7,
  i32 = 8,
  i64 = 9,
  i128 = 10,
  String = 11,
  bool = 12,
  Address = 13,
  Vec = 14,
  Map = 15,
  Set = 16,
  SizedByteArray = 17,
  Option = 18,
}

export type NumericTypeIndex = NumericTypeSpec["typeIndex"];

export type NumericTypeSpec =
  | U8TypeSpec
  | U16TypeSpec
  | U32TypeSpec
  | U64TypeSpec
  | U128TypeSpec
  | I8TypeSpec
  | I16TypeSpec
  | I32TypeSpec
  | I64TypeSpec
  | I128TypeSpec;

export interface U8TypeSpec {
  typeIndex: TypeIndex.u8;
}

export interface U16TypeSpec {
  typeIndex: TypeIndex.u16;
}

export interface U32TypeSpec {
  typeIndex: TypeIndex.u32;
}

export interface U64TypeSpec {
  typeIndex: TypeIndex.u64;
}

export interface U128TypeSpec {
  typeIndex: TypeIndex.u128;
}

export interface I8TypeSpec {
  typeIndex: TypeIndex.i8;
}

export interface I16TypeSpec {
  typeIndex: TypeIndex.i16;
}

export interface I32TypeSpec {
  typeIndex: TypeIndex.i32;
}

export interface I64TypeSpec {
  typeIndex: TypeIndex.i64;
}

export interface I128TypeSpec {
  typeIndex: TypeIndex.i128;
}

export type SimpleTypeIndex = SimpleTypeSpec["typeIndex"];

export type SimpleTypeSpec = NumericTypeSpec | StringTypeSpec | BoolTypeSpec | AddressTypeSpec;

export interface BoolTypeSpec {
  typeIndex: TypeIndex.bool;
}

export interface StringTypeSpec {
  typeIndex: TypeIndex.String;
}

export interface AddressTypeSpec {
  typeIndex: TypeIndex.Address;
}

export interface OptionTypeSpec {
  typeIndex: TypeIndex.Option;
  valueType: TypeSpec;
}

export interface VecTypeSpec {
  typeIndex: TypeIndex.Vec;
  valueType: TypeSpec;
}

export interface MapTypeSpec {
  typeIndex: TypeIndex.Map;
  keyType: TypeSpec;
  valueType: TypeSpec;
}

export interface SetTypeSpec {
  typeIndex: TypeIndex.Set;
  valueType: TypeSpec;
}

export interface SizedByteArrayTypeSpec {
  typeIndex: TypeIndex.SizedByteArray;
  length: number;
}
