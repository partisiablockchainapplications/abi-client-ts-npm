/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { AbstractReader } from "../AbstractReader";
import { FileAbi, FnKind, ShortnameType } from "../types/Abi";
import { FnKinds, RpcValueFn, ScValueMap, ScValueSet } from "../index";
import { BigEndianByteInput, BigEndianByteOutput } from "bitmanipulation-ts";

export class RpcReader extends AbstractReader {
  private readonly fileAbi: FileAbi;
  private readonly kind: FnKind;
  public static readonly ZK_OPEN_INVOCATION: number = 9;

  constructor(input: BigEndianByteInput | Buffer, fileAbi: FileAbi, kind: FnKind) {
    let byteInput;
    if ("length" in input) {
      byteInput = new BigEndianByteInput(input);
    } else {
      byteInput = new BigEndianByteInput(input.readRemaining());
    }
    super(byteInput, fileAbi.contract.namedTypes);
    this.fileAbi = fileAbi;
    this.kind = kind;
  }

  public readRpc(): RpcValueFn {
    if (this.fileAbi.contract.isZk() && this.kind === FnKinds.action) {
      const zkByte = this.input.readU8();
      if (zkByte !== RpcReader.ZK_OPEN_INVOCATION) {
        throw new Error(`First byte in RPC stream should be 9, but was ${zkByte}`);
      }
    }
    let shortname;
    if (this.fileAbi.format.shortnameType === ShortnameType.leb128) {
      shortname = this.parseLeb128();
      if (shortname.length > 5) {
        throw new Error(
          "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5 bytes)"
        );
      }
    } else {
      shortname = this.input.readBytes(this.fileAbi.format.shortnameLength);
    }
    const action = this.fileAbi.contract.getFunction(shortname, this.kind);
    if (action === undefined) {
      throw Error(`No action with shortname ${shortname.toString("hex")}`);
    }
    return new RpcValueFn(
      action,
      action.arguments.map((arg) => this.readGeneric(arg.type))
    );
  }

  private parseLeb128(): Buffer {
    let byte: number;
    const bufferWriter = new BigEndianByteOutput();
    do {
      byte = this.input.readU8();
      bufferWriter.writeU8(byte);
    } while (byte >= 128);
    return bufferWriter.toBuffer();
  }

  protected readMap(): ScValueMap {
    throw new Error("Type Map is not supported in rpc");
  }

  protected readSet(): ScValueSet {
    throw new Error("Type Set is not supported in rpc");
  }
}
