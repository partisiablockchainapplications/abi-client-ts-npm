/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { RpcValueFn } from "./RpcValueFn";
import { ValueJSON } from "../value/ValueJson";
import { JsonValueConverter } from "../value/JsonValueConverter";

export interface RpcValueJson {
  actionName: string;
  arguments: Record<string, ValueJSON>;
}

export const JsonRpcConverter = {
  toJson: (rpcValueFn: RpcValueFn): RpcValueJson => {
    const args: Record<string, ValueJSON> = {};
    rpcValueFn.fnAbi.arguments.forEach((argAbi, index) => {
      args[argAbi.name] = JsonValueConverter.toJson(rpcValueFn.arguments[index]);
    });
    return {
      actionName: rpcValueFn.fnAbi.name,
      arguments: args,
    };
  },
  toJsonString: (rpcValueFn: RpcValueFn): string =>
    JSON.stringify(JsonRpcConverter.toJson(rpcValueFn), null, 2),
};
