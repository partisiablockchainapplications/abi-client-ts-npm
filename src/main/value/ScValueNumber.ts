/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { NumericTypeIndex } from "../types/Abi";
import { ScValue } from "./ScValue";
import { BN } from "../index";

export class ScValueNumber extends ScValue {
  public readonly number: number | BN;
  public readonly type: NumericTypeIndex;

  constructor(type: NumericTypeIndex, value: number | BN) {
    super();
    this.type = type;
    this.number = value;
  }

  public getType() {
    return this.type;
  }

  public override asNumber(): number {
    if (typeof this.number === "number") {
      return this.number;
    } else {
      throw new Error("Cannot read number for current type");
    }
  }

  public override asBN(): BN {
    if (typeof this.number === "number") {
      return new BN(this.number);
    } else {
      return this.number;
    }
  }
}
