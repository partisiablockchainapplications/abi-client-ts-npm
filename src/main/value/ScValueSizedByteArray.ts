/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { ScValue } from "./ScValue";
import { TypeIndex } from "../types/Abi";

export class ScValueSizedByteArray extends ScValue {
  public readonly value: Buffer;

  constructor(value: Buffer) {
    super();
    this.value = value;
  }

  public getType(): TypeIndex {
    return TypeIndex.SizedByteArray;
  }

  public override sizedByteArrayValue(): Buffer {
    return this.value;
  }
}
