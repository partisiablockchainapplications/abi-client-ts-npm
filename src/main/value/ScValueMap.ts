/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { ScValue } from "./ScValue";
import { TypeIndex } from "../types/Abi";
import { HashMap } from "../HashMap";

export class ScValueMap extends ScValue {
  public map: HashMap<ScValue, ScValue>;

  constructor(map: HashMap<ScValue, ScValue>) {
    super();
    this.map = map;
  }

  public override mapValue(): ScValueMap {
    return this;
  }

  public getType(): TypeIndex {
    return TypeIndex.Map;
  }

  public get(key: ScValue): ScValue | undefined {
    return this.map.get(key);
  }

  public size(): number {
    return this.map.size;
  }

  public isEmpty(): boolean {
    return this.size() === 0;
  }
}
