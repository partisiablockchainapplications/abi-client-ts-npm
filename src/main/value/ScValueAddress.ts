/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { TypeIndex } from "../types/Abi";
import { ScValue } from "../index";

export class ScValueAddress extends ScValue {
  public readonly value: Buffer;

  constructor(value: Buffer) {
    super();
    if (value.length !== 21) {
      throw new Error(`Address expects exactly 21 bytes, but found ${value.length}`);
    }
    this.value = value;
  }

  public getType() {
    return TypeIndex.Address;
  }

  public override addressValue(): ScValueAddress {
    return this;
  }
}
