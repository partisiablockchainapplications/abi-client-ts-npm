/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { TypeIndex } from "../types/Abi";
import { ScValue } from "./ScValue";
import { ScValueEnum } from "./ScValueEnum";
import { ScValueMap } from "./ScValueMap";
import { ScValueOption } from "./ScValueOption";
import { ScValueSet } from "./ScValueSet";
import { ScValueStruct } from "./ScValueStruct";
import { ScValueVector } from "./ScValueVector";
import { ArrayJSON, MapJSON, OptionJSON, StructJSON, ValueJSON } from "./ValueJson";

// The maximum number of bytes shown when displaying Vec<u8>
const MAX_BYTES_SHOWN = 256;

export const JsonValueConverter = {
  toJson: (scValue: ScValue): ValueJSON => toJson(scValue),
  toJsonString: (scValue: ScValue): string => JSON.stringify(toJson(scValue), null, 2),
};

function toJson(scValue: ScValue): ValueJSON {
  const typeIndex = scValue.getType();

  if (typeIndex === TypeIndex.Named) {
    if (scValue instanceof ScValueStruct) {
      return toJsonStruct(scValue.structValue());
    } else {
      return toJsonEnum(scValue.enumValue());
    }
  } else if (typeIndex === TypeIndex.Vec) {
    return toJsonVec(scValue.vecValue());
  } else if (typeIndex === TypeIndex.Option) {
    return toJsonOption(scValue.optionValue());
  } else if (typeIndex === TypeIndex.Map) {
    return toJsonMap(scValue.mapValue());
  } else if (typeIndex === TypeIndex.Set) {
    return toJsonSet(scValue.setValue());
  } else if (typeIndex === TypeIndex.SizedByteArray) {
    return scValue.sizedByteArrayValue().toString("hex");
  } else if (typeIndex === TypeIndex.bool) {
    return scValue.boolValue();
  } else if (typeIndex === TypeIndex.Address) {
    return scValue.addressValue().value.toString("hex");
  } else if (typeIndex === TypeIndex.String) {
    return scValue.stringValue();
  } else {
    try {
      return scValue.asNumber();
    } catch (_) {
      return scValue.asBN().toString();
    }
  }
}

function toJsonStruct(scValueStruct: ScValueStruct): StructJSON {
  const map = scValueStruct.fieldMap;
  const result: Record<string, ValueJSON> = {};
  map.forEach((v, k) => {
    result[k] = toJson(v);
  });
  return result;
}

function toJsonEnum(scValueEnum: ScValueEnum): StructJSON {
  const scValueStruct = scValueEnum.item;
  const map = scValueStruct.fieldMap;
  const result: Record<string, ValueJSON> = {};
  result["@type"] = scValueStruct.name;
  map.forEach((v, k) => {
    result[k] = toJson(v);
  });
  return result;
}

function toJsonVec(scValueVector: ScValueVector): ArrayJSON | string {
  if (scValueVector.size() !== 0 && scValueVector.get(0).getType() === TypeIndex.u8) {
    if (scValueVector.size() > MAX_BYTES_SHOWN) {
      const values = scValueVector.values.slice(0, MAX_BYTES_SHOWN).map((v) => v.asNumber());
      return Buffer.from(values).toString("hex") + "...";
    } else {
      return Buffer.from(scValueVector.values.map((v) => v.asNumber())).toString("hex");
    }
  }
  return scValueVector.values.map((v) => toJson(v));
}

function toJsonOption(scValueOption: ScValueOption): OptionJSON {
  if (scValueOption.isSome() && scValueOption.innerValue !== null) {
    return { isSome: true, innerValue: toJson(scValueOption.innerValue) };
  } else {
    return { isSome: false };
  }
}

function toJsonMap(scValueMap: ScValueMap): MapJSON {
  const result: MapJSON = [];
  scValueMap.map.forEach((v, k) => {
    result.push({ key: toJson(k), value: toJson(v) });
  });
  return result;
}

function toJsonSet(scValueSet: ScValueSet): ArrayJSON {
  return scValueSet.values.map((v) => toJson(v));
}
