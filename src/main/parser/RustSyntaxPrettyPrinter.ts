/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import {
  AbiVersion,
  ArgumentAbi,
  EnumTypeSpec,
  EnumVariant,
  FieldAbi,
  FileAbi,
  FnAbi,
  MapTypeSpec,
  NamedTypeRef,
  NamedTypeSpec,
  OptionTypeSpec,
  SetTypeSpec,
  SizedByteArrayTypeSpec,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
} from "../index";
import { FnKinds } from "./FnKinds";

export class RustSyntaxPrettyPrinter {
  private readonly model;

  constructor(model: FileAbi) {
    this.model = model;
  }

  printModel(): string {
    const builder: string[] = [];
    // Build version information
    builder.push("// Version Binder: ");
    this.buildVersion(builder, this.model.versionBinder);
    builder.push("// Version Client: ");
    this.buildVersion(builder, this.model.versionClient);
    builder.push("\n");

    // Build list of struct definition
    const contract = this.model.contract;
    const stateStruct = contract.getStateStruct();
    const namedTypes = contract.namedTypes;
    for (const namedTypeSpec of namedTypes) {
      this.buildNamedDefinition(builder, namedTypeSpec, stateStruct);
    }

    for (const func of contract.functions) {
      this.buildFunction(builder, func);
    }

    return builder.join("");
  }

  printFunction(func: FnAbi): string {
    const builder: string[] = [];
    this.buildFunction(builder, func);
    return builder.join("");
  }

  printStruct(struct: StructTypeSpec): string {
    const builder: string[] = [];
    const stateStruct = this.model.contract.getStateStruct();
    this.buildStructDefinition(builder, struct, stateStruct);
    return builder.join("");
  }

  private buildVersion(builder: string[], version: AbiVersion) {
    builder.push(`${version.major}.${version.minor}.${version.patch}\n`);
  }

  private buildNamedDefinition(
    builder: string[],
    named: NamedTypeSpec,
    stateStruct: StructTypeSpec
  ) {
    if (named instanceof StructTypeSpec && !this.isEnumVariant(named as StructTypeSpec)) {
      this.buildStructDefinition(builder, named as StructTypeSpec, stateStruct);
    } else if (named instanceof EnumTypeSpec) {
      this.buildEnumDefinition(builder, named as EnumTypeSpec);
    }
  }

  private isEnumVariant(structTypeSpec: StructTypeSpec): boolean {
    const namedTypeSpecs = this.model.contract.namedTypes;
    for (const namedTypeSpec of namedTypeSpecs) {
      if (namedTypeSpec instanceof EnumTypeSpec) {
        const enumTypeSpec = namedTypeSpec as EnumTypeSpec;
        for (const variant of enumTypeSpec.variants) {
          if (this.model.contract.getNamedType(variant.def) === structTypeSpec) {
            return true;
          }
        }
      }
    }
    return false;
  }

  private buildEnumDefinition(builder: string[], enumTypeSpec: EnumTypeSpec) {
    builder.push("enum " + enumTypeSpec.name + " {\n");
    for (const variant of enumTypeSpec.variants) {
      this.buildEnumVariant(builder, variant);
    }
    builder.push("}\n");
  }

  private buildEnumVariant(builder: string[], variant: EnumVariant) {
    const namedTypeSpec = this.model.contract.getNamedType(variant.def) as StructTypeSpec;
    builder.push("    " + "#[discriminant(" + variant.discriminant + ")]\n");
    builder.push("    " + namedTypeSpec.name + " {");

    for (let i = 0; i < namedTypeSpec.fields.length; i++) {
      const field = namedTypeSpec.fields[i];
      builder.push(" " + field.name + ": ");
      this.buildType(builder, field.type);
      if (i < namedTypeSpec.fields.length - 1) {
        builder.push(",");
      } else {
        builder.push(" ");
      }
    }
    builder.push("},\n");
  }

  private buildStructDefinition(
    builder: string[],
    struct: StructTypeSpec,
    stateStruct: StructTypeSpec
  ) {
    if (stateStruct === struct) {
      builder.push("#[state]\n");
    }

    builder.push(`pub struct ${struct.name} {\n`);

    for (const field of struct.fields) {
      this.buildFields(builder, field);
    }
    builder.push("}\n");
  }

  private buildFields(builder: string[], field: FieldAbi) {
    builder.push(`    ${field.name}: `);
    this.buildType(builder, field.type);
    builder.push(",\n");
  }

  private buildType(builder: string[], type: TypeSpec) {
    const typeIndex = type.typeIndex;

    if (typeIndex === TypeIndex.Named) {
      this.buildStructRef(builder, type);
    } else if (typeIndex === TypeIndex.Vec) {
      this.buildVec(builder, type);
    } else if (typeIndex === TypeIndex.Option) {
      this.buildOption(builder, type);
    } else if (typeIndex === TypeIndex.Map) {
      this.buildMap(builder, type);
    } else if (typeIndex === TypeIndex.Set) {
      this.buildSet(builder, type);
    } else if (typeIndex === TypeIndex.SizedByteArray) {
      this.buildSizedByteArray(builder, type);
    } else {
      builder.push(TypeIndex[typeIndex]);
    }
  }

  private readonly FN_KIND_MACRO_NAMES = new Map<number, string>([
    [FnKinds.init.kindId, "init"],
    [FnKinds.action.kindId, "action"],
    [FnKinds.callback.kindId, "callback"],
    [FnKinds.zkSecretInput.kindId, "zk_secret_input"],
    [FnKinds.zkVarInputted.kindId, "zk_variable_inputted"],
    [FnKinds.zkVarRejected.kindId, "zk_variable_rejected"],
    [FnKinds.zkComputeComplete.kindId, "zk_compute_complete"],
    [FnKinds.zkVarOpened.kindId, "zk_variables_opened"],
    [FnKinds.zkUserVarOpened.kindId, "zk_user_variables_opened"],
    [FnKinds.zkAttestationComplete.kindId, "zk_on_attestation_complete"],
  ]);

  private buildFunction(builder: string[], func: FnAbi) {
    const kind = func.kind;
    builder.push(`#[${this.FN_KIND_MACRO_NAMES.get(kind.kindId)}`);
    if (kind === FnKinds.action || kind === FnKinds.callback || kind === FnKinds.zkSecretInput) {
      builder.push(`(shortname = 0x${func.shortname.toString("hex")})`);
    }
    builder.push("]\n");

    builder.push(`pub fn ${func.name} (`);
    if (func.arguments.length > 0) {
      builder.push("\n");
    }
    for (const arg of func.arguments) {
      this.buildArgument(builder, arg);
    }
    builder.push(")\n");
  }

  private buildArgument(builder: string[], arg: ArgumentAbi) {
    builder.push(`    ${arg.name}: `);
    this.buildType(builder, arg.type);
    builder.push(",\n");
  }

  private buildStructRef(builder: string[], type: NamedTypeRef) {
    builder.push(this.model.contract.getNamedType(type).name);
  }

  private buildVec(builder: string[], type: VecTypeSpec) {
    builder.push("Vec<");
    this.buildType(builder, type.valueType);
    builder.push(">");
  }

  private buildOption(builder: string[], type: OptionTypeSpec) {
    builder.push("Option<");
    this.buildType(builder, type.valueType);
    builder.push(">");
  }

  private buildMap(builder: string[], type: MapTypeSpec) {
    builder.push("Map<");
    this.buildType(builder, type.keyType);
    builder.push(", ");
    this.buildType(builder, type.valueType);
    builder.push(">");
  }

  private buildSet(builder: string[], type: SetTypeSpec) {
    builder.push("Set<");
    this.buildType(builder, type.valueType);
    builder.push(">");
  }

  private buildSizedByteArray(builder: string[], type: SizedByteArrayTypeSpec) {
    builder.push(`[u8; ${type.length}]`);
  }
}
