/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { FnKind } from "../types/Abi";

export const FnKinds = {
  init: { kindId: 0x01, minAllowedPerContract: 1, maxAllowedPerContract: 1 },
  action: { kindId: 0x02, minAllowedPerContract: 0, maxAllowedPerContract: 2147483647 },
  callback: { kindId: 0x03, minAllowedPerContract: 0, maxAllowedPerContract: 2147483647 },
  zkSecretInput: { kindId: 0x10, minAllowedPerContract: 0, maxAllowedPerContract: 2147483647 },
  zkVarInputted: { kindId: 0x11, minAllowedPerContract: 0, maxAllowedPerContract: 1 },
  zkVarRejected: { kindId: 0x12, minAllowedPerContract: 0, maxAllowedPerContract: 1 },
  zkComputeComplete: { kindId: 0x13, minAllowedPerContract: 0, maxAllowedPerContract: 1 },
  zkVarOpened: { kindId: 0x14, minAllowedPerContract: 0, maxAllowedPerContract: 1 },
  zkUserVarOpened: { kindId: 0x15, minAllowedPerContract: 0, maxAllowedPerContract: 1 },
  zkAttestationComplete: { kindId: 0x16, minAllowedPerContract: 0, maxAllowedPerContract: 1 },
};

const kindByValue: Map<number, FnKind> = new Map<number, FnKind>([
  [0x01, FnKinds.init],
  [0x02, FnKinds.action],
  [0x03, FnKinds.callback],
  [0x10, FnKinds.zkSecretInput],
  [0x11, FnKinds.zkVarInputted],
  [0x12, FnKinds.zkVarRejected],
  [0x13, FnKinds.zkComputeComplete],
  [0x14, FnKinds.zkVarOpened],
  [0x15, FnKinds.zkUserVarOpened],
  [0x16, FnKinds.zkAttestationComplete],
]);

export function fromKindId(id: number): FnKind | undefined {
  return kindByValue.get(id);
}
