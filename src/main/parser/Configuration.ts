/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import {
  AbiVersion,
  ConfigOption,
  FunctionFormat,
  NamedTypesFormat,
  ShortnameType,
} from "../types/Abi";

export class Configuration {
  public readonly shortnameType: ShortnameType;
  public readonly fnType: FunctionFormat;
  public readonly shortnameLength: number;
  public readonly namedTypesFormat: NamedTypesFormat;

  private static readonly supportedClientVersions: Map<string, ConfigOption> = new Map([
    [
      "1.0.0",
      {
        shortnameType: ShortnameType.hash,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "2.0.0",
      {
        shortnameType: ShortnameType.hash,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "3.0.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "3.1.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.InitSeparately,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "4.0.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "4.1.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.OnlyStructs,
      },
    ],
    [
      "5.0.0",
      {
        shortnameType: ShortnameType.leb128,
        fnType: FunctionFormat.FnKind,
        namedTypesFormat: NamedTypesFormat.StructsAndEnum,
      },
    ],
  ]);

  constructor(
    shortnameType: ShortnameType,
    fnType: FunctionFormat,
    shortnameLength: number,
    namedTypesFormat: NamedTypesFormat
  ) {
    this.shortnameType = shortnameType;
    this.fnType = fnType;
    this.shortnameLength = shortnameLength;
    this.namedTypesFormat = namedTypesFormat;
  }

  public static isClientVersionSupported(clientVersion: AbiVersion): ConfigOption | undefined {
    const zeroPatch = { major: clientVersion.major, minor: clientVersion.minor, patch: 0 };
    return this.supportedClientVersions.get(this.printVersion(zeroPatch));
  }

  public static printVersion(version: AbiVersion): string {
    return `${version.major}.${version.minor}.${version.patch}`;
  }
}
