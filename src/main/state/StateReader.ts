/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { AbstractReader } from "../AbstractReader";
import { ContractAbi } from "../parser/ContractAbi";
import {
  HashMap,
  MapTypeSpec,
  ScValue,
  ScValueMap,
  ScValueSet,
  SetTypeSpec,
  StructTypeSpec,
  TypeSpec,
} from "../index";
import { NamedTypeRef } from "../types/Abi";
import { LittleEndianByteInput } from "bitmanipulation-ts";

export class StateReader extends AbstractReader {
  private readonly contract: ContractAbi;

  constructor(input: LittleEndianByteInput | Buffer, contract: ContractAbi) {
    let byteInput;
    if ("length" in input) {
      byteInput = new LittleEndianByteInput(input);
    } else {
      byteInput = new LittleEndianByteInput(input.readRemaining());
    }
    super(byteInput, contract.namedTypes);
    this.contract = contract;
  }

  protected readSet(spec: SetTypeSpec): ScValueSet {
    const length = this.input.readU32();
    const vec: ScValue[] = [];
    for (let i = 0; i < length; i++) {
      vec.push(this.readGeneric(spec.valueType));
    }
    return new ScValueSet(vec);
  }

  protected readMap(spec: MapTypeSpec): ScValueMap {
    const stateMap = new HashMap<ScValue, ScValue>();
    const length = this.input.readU32();
    for (let i = 0; i < length; i++) {
      const key = this.readGeneric(spec.keyType);
      const value = this.readGeneric(spec.valueType);
      stateMap.set(key, value);
    }
    return new ScValueMap(stateMap);
  }

  public readState() {
    const structRef = this.contract.stateType as NamedTypeRef;
    const typeSpec = this.namedTypes[structRef.index] as StructTypeSpec;
    return this.readStruct(typeSpec);
  }

  public readStateValue(spec: TypeSpec): ScValue {
    return this.readGeneric(spec);
  }
}
