/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { ByteInput } from "bitmanipulation-ts";
import {
  BN,
  EnumTypeSpec,
  MapTypeSpec,
  NamedTypeRef,
  NamedTypeSpec,
  NumericTypeIndex,
  OptionTypeSpec,
  ScValueAddress,
  ScValueBool,
  ScValueEnum,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  SetTypeSpec,
  SimpleTypeIndex,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
} from "./index";
import { ScValue } from "./value/ScValue";

export abstract class AbstractReader {
  protected readonly input: ByteInput;
  protected readonly namedTypes: NamedTypeSpec[];

  protected constructor(input: ByteInput, namedTypes: NamedTypeSpec[]) {
    this.input = input;
    this.namedTypes = namedTypes;
  }

  protected readGeneric(spec: TypeSpec): ScValue {
    const typeIndex = spec.typeIndex;

    if (typeIndex === TypeIndex.Map) {
      return this.readMap(spec);
    } else if (typeIndex === TypeIndex.Set) {
      return this.readSet(spec);
    } else if (typeIndex === TypeIndex.Named) {
      return this.readNamedType(spec);
    } else if (typeIndex === TypeIndex.Vec) {
      return this.readVec(spec);
    } else if (typeIndex === TypeIndex.Option) {
      return this.readOptional(spec);
    } else if (typeIndex === TypeIndex.SizedByteArray) {
      return this.readSizedByteArray(spec.length);
    } else {
      return this.readSimpleType(typeIndex);
    }
  }

  public readSimpleType(typeIndex: SimpleTypeIndex): ScValue {
    if (typeIndex === TypeIndex.String) {
      const value = this.input.readString();
      return new ScValueString(value);
    } else if (typeIndex === TypeIndex.bool) {
      const value = this.input.readU8();
      return new ScValueBool(value !== 0);
    } else if (typeIndex === TypeIndex.Address) {
      const value = this.input.readBytes(21);
      return new ScValueAddress(value);
    } else {
      return new ScValueNumber(typeIndex, this.readNumber(typeIndex));
    }
  }

  protected readNumber(typeIndex: NumericTypeIndex): BN | number {
    if (typeIndex === TypeIndex.u8) {
      return this.input.readU8();
    } else if (typeIndex === TypeIndex.i8) {
      return this.input.readI8();
    } else if (typeIndex === TypeIndex.u16) {
      return this.input.readU16();
    } else if (typeIndex === TypeIndex.i16) {
      return this.input.readI16();
    } else if (typeIndex === TypeIndex.u32) {
      return this.input.readU32();
    } else if (typeIndex === TypeIndex.i32) {
      return this.input.readI32();
    } else if (typeIndex === TypeIndex.u64) {
      return this.input.readU64();
    } else if (typeIndex === TypeIndex.i64) {
      return this.input.readI64();
    } else if (typeIndex === TypeIndex.u128) {
      return this.input.readUnsignedBigInteger(16);
    } else {
      return this.input.readSignedBigInteger(16);
    }
  }

  protected readOptional(option: OptionTypeSpec): ScValueOption {
    const hasElement = this.input.readBoolean();
    if (hasElement) {
      return new ScValueOption(this.readGeneric(option.valueType));
    } else {
      return new ScValueOption(null);
    }
  }

  protected readVec(spec: VecTypeSpec): ScValueVector {
    if (spec.valueType.typeIndex === TypeIndex.u8) {
      const length = this.input.readU32();
      const bytes = this.input.readBytes(length);
      const values = Array.from(bytes).map((v) => new ScValueNumber(TypeIndex.u8, v));
      return new ScValueVector(values);
    } else {
      const length = this.input.readU32();
      const vec: ScValue[] = [];
      for (let i = 0; i < length; i++) {
        vec.push(this.readGeneric(spec.valueType));
      }
      return new ScValueVector(vec);
    }
  }

  protected readSizedByteArray(length: number): ScValueSizedByteArray {
    return new ScValueSizedByteArray(this.input.readBytes(length));
  }

  readNamedType(spec: NamedTypeRef): ScValueStruct | ScValueEnum {
    const typeSpec = this.namedTypes[spec.index];
    if (typeSpec instanceof StructTypeSpec) {
      return this.readStruct(typeSpec as StructTypeSpec);
    } else {
      return this.readEnum(typeSpec as EnumTypeSpec);
    }
  }

  readStruct(typeSpec: StructTypeSpec) {
    const fieldsMap = new Map<string, ScValue>();
    for (const field of typeSpec.fields) {
      const value = this.readGeneric(field.type);
      fieldsMap.set(field.name, value);
    }
    return new ScValueStruct(typeSpec.name, fieldsMap);
  }

  protected readEnum(typeSpec: EnumTypeSpec): ScValueEnum {
    const discriminant = this.input.readU8();
    for (const variant of typeSpec.variants) {
      if (variant.discriminant === discriminant) {
        const typeSpec = this.namedTypes[variant.def.index];
        return new ScValueEnum(typeSpec.name, this.readStruct(typeSpec as StructTypeSpec));
      }
    }
    throw new Error(`Undefined EnumVariant ${discriminant} used in ${typeSpec.name}`);
  }

  protected abstract readMap(spec: MapTypeSpec): ScValueMap;

  protected abstract readSet(spec: SetTypeSpec): ScValueSet;
}
