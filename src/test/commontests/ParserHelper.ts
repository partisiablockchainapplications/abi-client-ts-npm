/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import {
  AbiParser,
  ContractAbi,
  FnAbi,
  FnKind,
  FnKinds,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
} from "../../main";
import { NamedTypeSpec } from "../../main/";

export const ParserHelper = {
  assertFieldType(struct: NamedTypeSpec, index: number, fieldName: string, type: TypeSpec) {
    expect(struct instanceof StructTypeSpec);
    const fieldAbi = (struct as StructTypeSpec).fields[index];
    expect(fieldAbi.name).toEqual(fieldName);
    expect(fieldAbi.type).toEqual(type);
  },

  assertActionType(action: FnAbi | undefined, index: number, argName: string, type: TypeSpec) {
    const argument = action?.arguments[index];
    expect(argument?.name).toEqual(argName);
    expect(argument?.type).toEqual(type);
  },

  assertClientVersionFail(
    binderMajor: number,
    binderMinor: number,
    clientMajor: number,
    clientMinor: number
  ) {
    const bytesOne = Buffer.from([
      0x50,
      0x42,
      0x43,
      0x41,
      0x42,
      0x49,
      binderMajor,
      binderMinor,
      0x00,
      clientMajor,
      clientMinor,
      0x00,
    ]);

    const parserOne = new AbiParser(bytesOne);
    const expectedMessage = `Unsupported Version ${clientMajor}.${clientMinor}.0 for Version Client.`;

    expect(() => parserOne.parseAbi()).toThrowError(expectedMessage);
  },

  fnAbiWithKind(kind: FnKind): FnAbi {
    return {
      kind,
      name: "name",
      shortname: Buffer.from("abcdefab0f", "hex"),
      arguments: [{ name: "arg", type: { typeIndex: TypeIndex.i32 } }],
    };
  },

  contractFromKind(kind: FnKind): ContractAbi {
    return new ContractAbi(
      [],
      [
        { kind: FnKinds.action, name: "name", shortname: Buffer.from([1]), arguments: [] },
        { kind: FnKinds.init, name: "initialize", shortname: Buffer.from([0]), arguments: [] },
        { kind, name: "secret", shortname: Buffer.from([2]), arguments: [] },
      ],
      { typeIndex: TypeIndex.Named, index: 0 }
    );
  },
};
