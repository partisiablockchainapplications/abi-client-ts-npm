/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

const argument: ArgumentAbi = TestingHelper.argumentAbi(
  "arg1",
  TestingHelper.simpleTypeSpec(TypeIndex.i32)
);
const fnKindAbiInit: FnAbi = TestingHelper.fnAbi(FnKinds.init, "init", bytesFromHex("fffffffff0"), [
  argument,
]);
const simpleFnAbi: FnAbi = TestingHelper.fnAbi(
  FnKinds.action,
  "fnName",
  bytesFromHex("b0a1f1b30c"),
  [argument]
);

test("values", () => {
  expect(fnKindAbiInit.kind).toEqual(FnKinds.init);
  expect(fnKindAbiInit.name).toEqual("init");
  expect(fnKindAbiInit.shortname.toString("hex")).toEqual("fffffffff0");
  expect(fnKindAbiInit.arguments[0]).toEqual(argument);

  expect(simpleFnAbi.kind).toEqual(FnKinds.action);
  expect(simpleFnAbi.name).toEqual("fnName");
  expect(simpleFnAbi.shortname.toString("hex")).toEqual("b0a1f1b30c");
  expect(simpleFnAbi.arguments[0]).toEqual(argument);
});

test("testFnKindTypes", () => {
  const fnOne: FnAbi = ParserHelper.fnAbiWithKind(FnKinds.action);
  expect(fnOne.kind).toEqual(FnKinds.action);

  const fnTwo: FnAbi = ParserHelper.fnAbiWithKind(FnKinds.callback);
  expect(fnTwo.kind).toEqual(FnKinds.callback);

  const fnThree: FnAbi = ParserHelper.fnAbiWithKind(FnKinds.zkSecretInput);
  expect(fnThree.kind).toEqual(FnKinds.zkSecretInput);

  const fnFour: FnAbi = ParserHelper.fnAbiWithKind(FnKinds.zkVarInputted);
  expect(fnFour.kind).toEqual(FnKinds.zkVarInputted);

  const fnFive: FnAbi = ParserHelper.fnAbiWithKind(FnKinds.zkVarRejected);
  expect(fnFive.kind).toEqual(FnKinds.zkVarRejected);

  const fnSix: FnAbi = ParserHelper.fnAbiWithKind(FnKinds.zkComputeComplete);
  expect(fnSix.kind).toEqual(FnKinds.zkComputeComplete);

  const fnSeven: FnAbi = ParserHelper.fnAbiWithKind(FnKinds.zkVarOpened);
  expect(fnSeven.kind).toEqual(FnKinds.zkVarOpened);

  const fnEight: FnAbi = ParserHelper.fnAbiWithKind(FnKinds.zkUserVarOpened);
  expect(fnEight.kind).toEqual(FnKinds.zkUserVarOpened);

  const fn9: FnAbi = ParserHelper.fnAbiWithKind(FnKinds.zkAttestationComplete);
  expect(fn9.kind).toEqual(FnKinds.zkAttestationComplete);
});

test("tooManyInitFunctions", () => {
  const twoInitBytes = bytesFromHex(
    // Header bytes
    "504243414249030000040000" +
      // Zero structs
      "00000000" +
      // Two functions
      "00000002" +
      // FnKind: Init, name = init, shortname = 00, 0 arguments
      "01" +
      "00000004696e697400" +
      "00000000" +
      // FnKind: Init, name = initialize, shortname = 01, 0 arguments
      "01" +
      "0000000a696e697469616c697a5401" +
      "00000000"
  );

  const parser: AbiParser = new AbiParser(twoInitBytes);
  expect(() => parser.parseAbi()).toThrowError(
    "Expected between 1 and 1 Init functions, but found 2"
  );
});

test("tooFewInitFunctions", () => {
  const twoInitBytes = bytesFromHex(
    // Header bytes
    "504243414249030000040000" +
      // Zero structs
      "00000000" +
      // Zero functions
      "00000000" +
      // State type: u8
      "01"
  );

  const parser: AbiParser = new AbiParser(twoInitBytes);
  expect(() => parser.parseAbi()).toThrowError(
    "Expected between 1 and 1 Init functions, but found 0"
  );
});

test("tooManyZkVarInputtedFunctions", () => {
  const twoZkFnKindBytes = bytesFromHex(
    // Header bytes
    "504243414249030000040000" +
      // Zero structs
      "00000000" +
      // Three functions
      "00000003" +
      // FnKind: Init, name = init, shortname = 00, 0 arguments
      "01" +
      "00000004696e697400" +
      "00000000" +
      // FnKind: ZkVarInputted, name = myfn, shortname = 01, 0 arguments
      "11" +
      "000000046d79666e01" +
      "00000000" +
      // FnKind: ZkVarInputted, name = fntwo, shortname = 02, 0 arguments
      "11" +
      "00000005666e74776f02" +
      "00000000"
  );
  const parserZkVarInputted: AbiParser = new AbiParser(twoZkFnKindBytes);
  expect(() => parserZkVarInputted.parseAbi()).toThrowError(
    "Expected between 0 and 1 ZkVarInputted functions, but found 2"
  );
});
