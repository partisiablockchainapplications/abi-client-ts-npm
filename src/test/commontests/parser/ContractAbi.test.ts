/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

const abi: ContractAbi = new ContractAbi(
  [],
  list(
    TestingHelper.fnAbi(FnKinds.action, "name", Buffer.from([0x01]), []),
    TestingHelper.fnAbi(FnKinds.init, "initialize", Buffer.from([0x00]), [])
  ),
  TestingHelper.simpleTypeSpec(TypeIndex.u64)
);

test("getFunctionByName", () => {
  expect(requireNonNull(abi.getFunctionByName("name")).name).toEqual("name");
  expect(abi.getFunctionByName("x")).toBeUndefined();
});

test("getFunctionByShortname", () => {
  expect(requireNonNull(abi.getFunction(Buffer.from([0x01]), FnKinds.action)).name).toEqual("name");
  expect(abi.getFunction(Buffer.from([0x02]), FnKinds.action)).toBeUndefined();
});

test("init", () => {
  expect(requireNonNull(abi.init()).name).toEqual("initialize");
  expect(requireNonNull(abi.init()).shortname).toEqual(Buffer.from([0x00]));
});

test("getStateStruct", () => {
  const stateStruct = new StructTypeSpec(
    "test",
    list(TestingHelper.fieldAbi("f1", TestingHelper.simpleTypeSpec(TypeIndex.bool)))
  );
  const contractAbi = new ContractAbi(
    [stateStruct],
    list(
      TestingHelper.fnAbi(FnKinds.action, "name", Buffer.from([0x01]), []),
      TestingHelper.fnAbi(FnKinds.init, "initialize", Buffer.from([0x00]), [])
    ),
    TestingHelper.namedTypeRef(0)
  );
  expect(contractAbi.getStateStruct()).toEqual(stateStruct);
});

test("isZk", () => {
  expect(abi.isZk()).toBeFalsy();
  expect(ParserHelper.contractFromKind(FnKinds.zkSecretInput).isZk()).toBeTruthy();
  expect(ParserHelper.contractFromKind(FnKinds.zkComputeComplete).isZk()).toBeTruthy();
  expect(ParserHelper.contractFromKind(FnKinds.zkUserVarOpened).isZk()).toBeTruthy();
  expect(ParserHelper.contractFromKind(FnKinds.zkVarInputted).isZk()).toBeTruthy();
  expect(ParserHelper.contractFromKind(FnKinds.zkVarOpened).isZk()).toBeTruthy();
  expect(ParserHelper.contractFromKind(FnKinds.zkVarRejected).isZk()).toBeTruthy();
  expect(ParserHelper.contractFromKind(FnKinds.zkAttestationComplete).isZk()).toBeTruthy();
});
