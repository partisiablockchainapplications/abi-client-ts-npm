/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("contractBooleans", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
  const model = TestingHelper.loadAbiParserFromFile("contract_booleans.abi").parseAbi();
  expect(AbiParser.printModel(model)).toEqual(
    "// Version Binder: 1.0.0\n" +
      "// Version Client: 1.0.0\n" +
      "\n" +
      "#[state]\n" +
      "pub struct ExampleContractState {\n" +
      "    my_bool: bool,\n" +
      "}\n" +
      "#[init]\n" +
      "pub fn initialize (\n" +
      "    my_bool: bool,\n" +
      ")\n" +
      "#[action(shortname = 0xc67e90b0)]\n" +
      "pub fn update_my_bool (\n" +
      "    value: bool,\n" +
      ")\n"
  );
});

test("contractSimpleMap", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-map
  const model = TestingHelper.loadAbiParserFromFile("contract_simple_map.abi").parseAbi();
  expect(AbiParser.printModel(model)).toEqual(
    "// Version Binder: 1.0.0\n" +
      "// Version Client: 1.0.0\n" +
      "\n" +
      "#[state]\n" +
      "pub struct ExampleContractState {\n" +
      "    my_map: Map<Address, u8>,\n" +
      "}\n" +
      "#[init]\n" +
      "pub fn initialize ()\n" +
      "#[action(shortname = 0x282d08fc)]\n" +
      "pub fn insert_in_my_map (\n" +
      "    address: Address,\n" +
      "    value: u8,\n" +
      ")\n"
  );
});

test("contractOptions", () => {
  const model = TestingHelper.loadAbiParserFromFile("contract_options.abi").parseAbi();
  expect(AbiParser.printModel(model)).toEqual(
    "// Version Binder: 1.1.0\n" +
      "// Version Client: 2.0.0\n" +
      "\n" +
      "#[state]\n" +
      "pub struct ExampleContractState {\n" +
      "    option_u64: Option<u64>,\n" +
      "    option_string: Option<String>,\n" +
      "    option_address: Option<Address>,\n" +
      "    option_boolean: Option<bool>,\n" +
      "    option_map: Option<Map<u64, u64>>,\n" +
      "}\n" +
      "#[init]\n" +
      "pub fn initialize (\n" +
      "    option_u64: Option<u64>,\n" +
      "    option_string: Option<String>,\n" +
      "    option_address: Option<Address>,\n" +
      "    option_boolean: Option<bool>,\n" +
      ")\n" +
      "#[action(shortname = 0xbd3fce4f)]\n" +
      "pub fn update_u64 (\n" +
      "    value: Option<u64>,\n" +
      ")\n" +
      "#[action(shortname = 0x871a7573)]\n" +
      "pub fn update_string (\n" +
      "    value: Option<String>,\n" +
      ")\n" +
      "#[action(shortname = 0x1d2c55ac)]\n" +
      "pub fn update_address (\n" +
      "    value: Option<Address>,\n" +
      ")\n" +
      "#[action(shortname = 0x6b53c77c)]\n" +
      "pub fn update_boolean (\n" +
      "    value: Option<bool>,\n" +
      ")\n" +
      "#[action(shortname = 0xcc0ce5db)]\n" +
      "pub fn add_entry_to_map (\n" +
      "    key: u64,\n" +
      "    value: u64,\n" +
      ")\n"
  );
});

test("contractSet", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-set
  const model = TestingHelper.loadAbiParserFromFile("contract_set.abi").parseAbi();
  expect(AbiParser.printModel(model)).toEqual(
    "// Version Binder: 1.1.0\n" +
      "// Version Client: 2.0.0\n" +
      "\n" +
      "#[state]\n" +
      "pub struct ExampleContractState {\n" +
      "    my_set: Set<u64>,\n" +
      "}\n" +
      "#[init]\n" +
      "pub fn initialize ()\n" +
      "#[action(shortname = 0x4bfabd75)]\n" +
      "pub fn insert_in_my_set (\n" +
      "    value: u64,\n" +
      ")\n"
  );
});

test("contractByteArrays", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-byte-arrays
  const model = TestingHelper.loadAbiParserFromFile("contract_byte_arrays.abi").parseAbi();
  expect(AbiParser.printModel(model)).toEqual(
    "// Version Binder: 1.1.0\n" +
      "// Version Client: 2.0.0\n" +
      "\n" +
      "#[state]\n" +
      "pub struct ExampleContractState {\n" +
      "    my_array: [u8; 16],\n" +
      "    my_array_2: [u8; 5],\n" +
      "}\n" +
      "#[init]\n" +
      "pub fn initialize (\n" +
      "    my_array: [u8; 16],\n" +
      "    my_array_2: [u8; 5],\n" +
      ")\n" +
      "#[action(shortname = 0xdfb2f24b)]\n" +
      "pub fn update_my_array (\n" +
      "    value: [u8; 16],\n" +
      ")\n" +
      "#[action(shortname = 0x0ea9ceec)]\n" +
      "pub fn update_my_array_2 (\n" +
      "    value: [u8; 5],\n" +
      ")\n"
  );
});

test("testStringifier", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-vector
  const model = TestingHelper.loadAbiParserFromFile("contract_simple_vector.abi").parseAbi();
  expect(AbiParser.printModel(model)).toEqual(
    "// Version Binder: 1.1.0\n" +
      "// Version Client: 2.0.0\n" +
      "\n" +
      "#[state]\n" +
      "pub struct ExampleContractState {\n" +
      "    my_vec: Vec<u64>,\n" +
      "}\n" +
      "#[init]\n" +
      "pub fn initialize (\n" +
      "    my_vec: Vec<u64>,\n" +
      ")\n" +
      "#[action(shortname = 0x1c998c9d)]\n" +
      "pub fn update_my_vec (\n" +
      "    value: Vec<u64>,\n" +
      ")\n"
  );
});

test("contractStructOfStruct", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct/src
  const model = TestingHelper.loadAbiParserFromFile("contract_struct_of_struct_v3.abi").parseAbi();
  expect(AbiParser.printModel(model)).toEqual(
    "// Version Binder: 3.0.0\n" +
      "// Version Client: 3.0.0\n" +
      "\n" +
      "pub struct MyStructType {\n" +
      "    some_value: u64,\n" +
      "    some_vector: Vec<u64>,\n" +
      "}\n" +
      "pub struct MyOtherStructType {\n" +
      "    value: u64,\n" +
      "    vector: Vec<MyStructType>,\n" +
      "}\n" +
      "#[state]\n" +
      "pub struct ExampleContractState {\n" +
      "    my_other_struct: MyOtherStructType,\n" +
      "}\n" +
      "#[init]\n" +
      "pub fn initialize (\n" +
      "    my_other_struct: MyOtherStructType,\n" +
      ")\n" +
      "#[action(shortname = 0x85f292be0b)]\n" +
      "pub fn update_my_other_struct (\n" +
      "    value: MyOtherStructType,\n" +
      ")\n"
  );
});

test("contractEnum", () => {
  // Contract source can be found at:
  // https://gitlab.com/secata/pbc/language/contracts/testing-types/-/tree/main/contract-enum
  const model = TestingHelper.loadAbiParserFromFile("contract_enum.abi").parseAbi();
  expect(AbiParser.printModel(model)).toEqual(
    "// Version Binder: 8.1.0\n" +
      "// Version Client: 5.0.0\n" +
      "\n" +
      "enum Vehicle {\n" +
      "    #[discriminant(2)]\n" +
      "    Bicycle { wheel_diameter: i32 },\n" +
      "    #[discriminant(5)]\n" +
      "    Car { engine_size: u8, supports_trailer: bool },\n" +
      "}\n" +
      "#[state]\n" +
      "pub struct EnumContractState {\n" +
      "    my_enum: Vehicle,\n" +
      "}\n" +
      "pub struct Signature {\n" +
      "    recovery_id: u8,\n" +
      "    value_r: [u8; 32],\n" +
      "    value_s: [u8; 32],\n" +
      "}\n" +
      "#[init]\n" +
      "pub fn initialize (\n" +
      "    my_enum: Vehicle,\n" +
      ")\n" +
      "#[action(shortname = 0xffffffff0d)]\n" +
      "pub fn update_enum (\n" +
      "    val: Vehicle,\n" +
      ")\n"
  );
});

test("testLebShortnameStringify", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-callbacks/src
  const model = TestingHelper.loadAbiParserFromFile("contract_callbacks_v3.abi").parseAbi();
  expect(AbiParser.printModel(model)).toEqual(
    "// Version Binder: 3.0.0\n" +
      "// Version Client: 3.0.0\n" +
      "\n" +
      "#[state]\n" +
      "pub struct ExampleContractState {\n" +
      "    val: u32,\n" +
      "    successful_callback: bool,\n" +
      "    callback_results: Vec<bool>,\n" +
      "    callback_value: u32,\n" +
      "}\n" +
      "#[init]\n" +
      "pub fn initialize (\n" +
      "    val: u32,\n" +
      ")\n" +
      "#[action(shortname = 0x01)]\n" +
      "pub fn set_and_callback (\n" +
      "    value: u32,\n" +
      ")\n" +
      "#[action(shortname = 0x02)]\n" +
      "pub fn panic_and_callback ()\n" +
      "#[action(shortname = 0x03)]\n" +
      "pub fn panicking ()\n" +
      "#[action(shortname = 0x04)]\n" +
      "pub fn set (\n" +
      "    value: u32,\n" +
      ")\n" +
      "#[action(shortname = 0x06)]\n" +
      "pub fn multiple_events_and_callback (\n" +
      "    value: u32,\n" +
      ")\n"
  );
});

test("printFunctionOnly", () => {
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_set.abi");
  const abi: FileAbi = parser.parseAbi();
  expect(AbiParser.printFunction(abi, requireNonNull(abi.contract.init()))).toEqual(
    "#[init]\n" + "pub fn initialize ()\n"
  );
});

test("printStructOnly", () => {
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_callbacks_v3.abi");
  const abi: FileAbi = parser.parseAbi();
  const structTypeSpec: StructTypeSpec = abi.contract.namedTypes[0] as StructTypeSpec;
  expect(AbiParser.printStruct(abi, structTypeSpec)).toEqual(
    "#[state]\n" +
      "pub struct ExampleContractState {\n" +
      "    val: u32,\n" +
      "    successful_callback: bool,\n" +
      "    callback_results: Vec<bool>,\n" +
      "    callback_value: u32,\n" +
      "}\n"
  );
});
