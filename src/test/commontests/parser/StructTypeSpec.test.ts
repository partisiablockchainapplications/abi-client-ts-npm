/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

const field: FieldAbi = TestingHelper.fieldAbi(
  "field0",
  TestingHelper.simpleTypeSpec(TypeIndex.u8)
);
const struct: StructTypeSpec = new StructTypeSpec("name", [field]);
const structType: NamedTypeRef = TestingHelper.namedTypeRef(0);

test("values", () => {
  expect(struct.name).toEqual("name");
  expect(struct.fields).toHaveLength(1);
  expect(structType.typeIndex).toEqual(TypeIndex.Named);
  expect(struct.fields[0]).toBe(field);
});

test("structTypeSpecValues", () => {
  const structOne: StructTypeSpec = new StructTypeSpec("structOne", [field]);
  const structTwo: StructTypeSpec = new StructTypeSpec("structTwo", []);
  const structAbiList: NamedTypeSpec[] = [structOne, structTwo];
  const structTypeOne: NamedTypeRef = TestingHelper.namedTypeRef(0);
  const structTypeTwo: NamedTypeRef = TestingHelper.namedTypeRef(1);
  const contract: ContractAbi = new ContractAbi(structAbiList, [], structTypeOne);

  expect(contract.getNamedType(structTypeOne)).toEqual(structOne);
  expect(structTypeOne.index).toEqual(0);
  expect(contract.getNamedType(structTypeTwo)).toEqual(structTwo);
  expect(structTypeTwo.index).toEqual(1);
});

test("testMutualRecursion", () => {
  const typeOne: NamedTypeRef = TestingHelper.namedTypeRef(0);
  const typeTwo: NamedTypeRef = TestingHelper.namedTypeRef(1);
  const structOne: StructTypeSpec = new StructTypeSpec(
    "type2",
    list(TestingHelper.fieldAbi("arg1", typeTwo))
  );
  const structTwo: StructTypeSpec = new StructTypeSpec(
    "type1",
    list(TestingHelper.fieldAbi("arg2", typeOne))
  );

  expect(structTwo.fields[0].type).toEqual(typeOne);
  expect(structOne.fields[0].type).toEqual(typeTwo);
});

test("testSelfRecursion", () => {
  const selfRecursiveTypeSpec: NamedTypeRef = TestingHelper.namedTypeRef(0);
  const struct: StructTypeSpec = new StructTypeSpec(
    "SelfRecursiveStruct",
    list(TestingHelper.fieldAbi("argument", selfRecursiveTypeSpec))
  );
  expect(struct.fields[0].type).toEqual(selfRecursiveTypeSpec);
});
