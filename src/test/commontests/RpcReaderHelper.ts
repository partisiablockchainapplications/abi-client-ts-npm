/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import {
  AbiVersion,
  Configuration,
  ContractAbi,
  FileAbi,
  FnKinds,
  FunctionFormat,
  NamedTypesFormat,
  RpcReader,
  ShortnameType,
  TypeIndex,
  TypeSpec,
} from "../../main";
import { BigEndianByteInput } from "bitmanipulation-ts";

export const RpcReaderHelper = {
  wrap(rpcHex: string, arg: FileAbi | TypeSpec): RpcReader {
    let fileAbi;
    if ("typeIndex" in arg) {
      const contract = new ContractAbi(
        [],
        [
          {
            kind: FnKinds.action,
            name: "test",
            shortname: Buffer.from("dddddd0d", "hex"),
            arguments: [{ name: "value", type: arg }],
          },
        ],
        { typeIndex: TypeIndex.Named, index: 0 }
      );
      const version: AbiVersion = { major: 4, minor: 0, patch: 0 };
      const format = new Configuration(
        ShortnameType.leb128,
        FunctionFormat.FnKind,
        -1,
        NamedTypesFormat.OnlyStructs
      );
      fileAbi = {
        header: "PBCABI",
        versionBinder: version,
        versionClient: version,
        format,
        contract,
      };
    } else {
      fileAbi = arg;
    }
    return new RpcReader(
      new BigEndianByteInput(Buffer.from(rpcHex, "hex")),
      fileAbi,
      FnKinds.action
    );
  },
};
