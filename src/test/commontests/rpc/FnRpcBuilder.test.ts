/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("buildEverything", () => {
  const builder = new FnRpcBuilder(bytesFromHex("fffffffff0"));
  const fn = (b: AbstractBuilder<BigEndianByteOutput>) =>
    b
      .addI8(-128)
      .addU8(128)
      .addI16(-32768)
      .addU16(32767)
      .addI32(-2147483648)
      .addU32(2147483647)
      .addI64(new BN("-9223372036854775808"))
      .addU64(new BN("9223372036854775807"))
      .addI128(new BN("-170141183460469231731687303715884105728"))
      .addU128(new BN("170141183460469231731687303715884105727"))
      .addString("Test string")
      .addBool(true)
      .addBool(false)
      .addOption()
      .addU64(42);

  fn(builder);
  fn(builder.addStruct());
  fn(builder.addVec());
  fn(builder.addSet());

  const actual = BuilderHelper.builderToBytesBe(builder);
  expect(actual).toEqual(
    bytesFromHex(
      "" +
        "fffffffff0808080007fff800000007fffffff" +
        "80000000000000007fffffffffffffff800000" +
        "000000000000000000000000007fffffffffff" +
        "ffffffffffffffffffff0000000b5465737420" +
        "737472696e67010001000000000000002a8080" +
        "80007fff800000007fffffff80000000000000" +
        "007fffffffffffffff80000000000000000000" +
        "0000000000007fffffffffffffffffffffffff" +
        "ffffff0000000b5465737420737472696e6701" +
        "0001000000000000002a0000000e808080007f" +
        "ff800000007fffffff80000000000000007fff" +
        "ffffffffffff80000000000000000000000000" +
        "0000007fffffffffffffffffffffffffffffff" +
        "0000000b5465737420737472696e6701000100" +
        "0000000000002a0000000e808080007fff8000" +
        "00007fffffff80000000000000007fffffffff" +
        "ffffff80000000000000000000000000000000" +
        "7fffffffffffffffffffffffffffffff000000" +
        "0b5465737420737472696e67010001000000000" +
        "000002a"
    )
  );
});

test("simpleBoolTest", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("contract_booleans_v3.abi");
  const model: FileAbi = parser.parseAbi();
  const contract: ContractAbi = model.contract;
  const shortname = bytesFromHex("b0a1fab30c");

  const rpc = BuilderHelper.builderToBytesBe(
    new FnRpcBuilder(shortname, contract, FnKinds.action).addBool(false)
  );
  const expected = concatBytes(shortname, bytesFromHex("00"));
  expect(rpc).toEqual(expected);

  const builderTrue: FnRpcBuilder = new FnRpcBuilder(shortname, contract, FnKinds.action);
  const rpcTrue = BuilderHelper.builderToBytesBe(builderTrue.addBool(true));
  const expectedTrue = concatBytes(shortname, bytesFromHex("01"));
  expect(rpcTrue).toEqual(expectedTrue);
});

test("contractAddress", () => {
  const address = "b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff";

  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-address
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_address_v3.abi",
    "update_my_address"
  );
  const rpc = BuilderHelper.builderToBytesBe(builder.addAddress(bytesFromHex(address)));
  const expected = concatBytes(bytesFromHex("a7e192a009"), bytesFromHex(address));
  expect(rpc).toEqual(expected);

  const invalidBuilder = TestingHelper.createBuilderFromFile(
    "contract_address_v3.abi",
    "update_my_address"
  );
  expect(() =>
    BuilderHelper.builderToBytesBe(invalidBuilder.addAddress(Buffer.alloc(4)))
  ).toThrowError("Address must have length 21 bytes, got length = 4, value = 00000000");
});

test("assertTypeError", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-address
  const builder = TestingHelper.createBuilderFromFile(
    "contract_address_v3.abi",
    "update_my_address"
  );
  expect(() => BuilderHelper.builderToBytesBe(builder.addBool(false))).toThrowError(
    "In update_my_address/value, Expected type Address, but got bool"
  );
});

test("contractString", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-strings
  const builder = TestingHelper.createBuilderFromFile(
    "contract_strings_v3.abi",
    "update_my_string"
  );
  const rpc = BuilderHelper.builderToBytesBe(builder.addString("hello"));
  expect(rpc).toEqual(concatBytes(bytesFromHex("97c18ca406"), bytesFromStringBe("hello")));
});

test("complexStructure", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-map-of-struct
  const builder = TestingHelper.createBuilderFromFile(
    "contract_map_of_struct_v3.abi",
    "insert_in_my_map"
  );
  builder.addString("keyType").addStruct().addU64(2).addVec().addU64(3);
  const rpc = BuilderHelper.builderToBytesBe(builder);
  const expected = concatBytes(
    bytesFromHex("fc91b4c102"),
    bytesFromStringBe("keyType"),
    bytesFromHex("0000000000000002"),
    // 0001 -=> one element, u64 with valueType 3
    bytesFromHex("00000001"),
    bytesFromHex("0000000000000003")
  );
  expect(rpc).toEqual(expected);
});

test("mapProducer", () => {
  const mapType = TestingHelper.mapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.String),
    TestingHelper.simpleTypeSpec(TypeIndex.u64)
  );

  const mapEntryType = new StructTypeSpec(
    "MapEntry$String$u64",
    list(
      TestingHelper.fieldAbi("keyType", mapType.keyType),
      TestingHelper.fieldAbi("valueType", mapType.valueType)
    )
  );

  const arg = TestingHelper.argumentAbi("valueType", mapType);
  const fn = TestingHelper.fnAbi(FnKinds.action, "action", Buffer.alloc(1), [arg]);

  const contract = new ContractAbi([mapEntryType], [fn], TestingHelper.namedTypeRef(0));
  const builder = new FnRpcBuilder("action", contract);
  const mapProducer = builder.addMap();
  mapProducer.addString("key1").addU64(4);
  mapProducer.addString("key2").addU64(8);

  const bytes = BuilderHelper.builderToBytesBe(builder);
  const expected = concatBytes(
    bytesFromHex("0000000002"),
    bytesFromStringBe("key1"),
    bytesFromHex("0000000000000004"),
    bytesFromStringBe("key2"),
    bytesFromHex("0000000000000008")
  );
  expect(bytes).toEqual(expected);
});

test("buildVecU8", () => {
  const arg = TestingHelper.argumentAbi(
    "valueType",
    TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u8))
  );
  const fn = TestingHelper.fnAbi(FnKinds.action, "action", Buffer.alloc(1), [arg]);
  const contract = new ContractAbi([], [fn], TestingHelper.namedTypeRef(0));
  const builder1 = new FnRpcBuilder("action", contract);
  const builder2 = new FnRpcBuilder("action", contract);
  builder1.addVec().addU8(1).addU8(2).addU8(3);
  builder2.addVecU8(Buffer.from([1, 2, 3]));
  expect(BuilderHelper.builderToBytesBe(builder1)).toEqual(
    BuilderHelper.builderToBytesBe(builder2)
  );

  const noTypeCheckBuilder1 = new FnRpcBuilder(Buffer.alloc(1));
  const noTypeCheckBuilder2 = new FnRpcBuilder(Buffer.alloc(1));
  noTypeCheckBuilder1.addVec().addU8(1).addU8(2).addU8(3);
  noTypeCheckBuilder2.addVecU8(Buffer.from([1, 2, 3]));
  expect(BuilderHelper.builderToBytesBe(noTypeCheckBuilder1)).toEqual(
    BuilderHelper.builderToBytesBe(noTypeCheckBuilder2)
  );

  const returnBuilder = new FnRpcBuilder(Buffer.alloc(1));
  returnBuilder.addVecU8(Buffer.from([1])).addVecU8(Buffer.from([2]));
  expect(BuilderHelper.builderToBytesBe(returnBuilder)).toEqual(
    bytesFromHex("0000000001010000000102")
  );
});

test("buildVecU8TypeError", () => {
  const arg1 = TestingHelper.argumentAbi("bool", TestingHelper.simpleTypeSpec(TypeIndex.bool));
  const arg2 = TestingHelper.argumentAbi(
    "vec",
    TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u16))
  );
  const fn = TestingHelper.fnAbi(FnKinds.action, "action", Buffer.alloc(1), [arg1, arg2]);
  const contract = new ContractAbi([], [fn], TestingHelper.namedTypeRef(0));

  const builder = new FnRpcBuilder("action", contract);
  expect(() => builder.addVecU8(Buffer.alloc(1))).toThrowError(
    "In action/bool, Expected type bool, but got Vec"
  );
  builder.addBool(true);
  expect(() => builder.addVecU8(Buffer.alloc(1))).toThrowError(
    "In action/vec, Expected type Vec<u16>, but got Vec<u8>"
  );
});

test("shortnameHashContract", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
  const builderFalse: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_booleans.abi",
    "update_my_bool"
  );
  const rpc = BuilderHelper.builderToBytesBe(builderFalse.addBool(false));
  const expected = bytesFromHex("c67e90b0" + "00"); // HASH Shortname for update_my_bool + 0x00 since false
  expect(rpc).toEqual(expected);

  const builderTrue: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_booleans.abi",
    "update_my_bool"
  );
  const rpcTrue = BuilderHelper.builderToBytesBe(builderTrue.addBool(true));
  const expectedTrue = bytesFromHex("c67e90b0" + "01");
  expect(rpcTrue).toEqual(expectedTrue);
});

test("canDelayGeneration", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-nested-vector
  const builder = TestingHelper.createBuilderFromFile(
    "contract_nested_vector_v3.abi",
    "update_my_vec_vec"
  );
  const outerVecRpcProducer = builder.addVec();
  const innerVecRpcProducer = outerVecRpcProducer.addVec(); // First outer
  outerVecRpcProducer.addVec().addU64(1); // Second outer
  innerVecRpcProducer.addU64(10).addU64(11);
  const rpc = BuilderHelper.builderToBytesBe(builder);

  // We expect the following: [[10, 11]], [1]]
  const expectedBytes = concatBytes(
    bytesFromHex("ccc0d6db04"),
    // Two elements in outer vec
    bytesFromHex("00000002"),
    // First valueType has 10, 11
    bytesFromHex("00000002" + "000000000000000A000000000000000B"),
    bytesFromHex("000000010000000000000001")
  );
  expect(rpc).toEqual(expectedBytes);
});

test("noTypeChecking", () => {
  const builder = new FnRpcBuilder(Buffer.alloc(4));
  const rpc = BuilderHelper.builderToBytesBe(builder.addBool(false).addU64(2).addString("hello"));
  const expected = bytesFromHex("000000000000000000000000020000000568656c6c6f");
  expect(rpc).toEqual(expected);
});

test("toFewArguments", () => {
  const arg = TestingHelper.argumentAbi("valueType", TestingHelper.simpleTypeSpec(TypeIndex.u8));
  const fn = TestingHelper.fnAbi(FnKinds.action, "fn2", Buffer.alloc(1), [arg]);

  const contractAbi = new ContractAbi([], [fn], TestingHelper.namedTypeRef(0));

  expect(() => BuilderHelper.builderToBytesBe(new FnRpcBuilder("fn2", contractAbi))).toThrowError(
    "Missing argument 'valueType'"
  );
});

test("tooManyArguments", () => {
  const arg = TestingHelper.argumentAbi("field1", TestingHelper.simpleTypeSpec(TypeIndex.u8));
  const fn = TestingHelper.fnAbi(FnKinds.action, "fn1", Buffer.alloc(1), [arg]);

  const contractAbi = new ContractAbi([], [fn], TestingHelper.namedTypeRef(0));

  const builder = new FnRpcBuilder("fn1", contractAbi);
  builder.addU8(0);

  expect(() => builder.addVec()).toThrowError("Cannot add more arguments than the action expects.");
});

test("addSizedByteArrayWithType", () => {
  const spec = TestingHelper.sizedByteArrayTypeSpec(1);
  const fnAbiWithType = TestingHelper.fnAbi(
    FnKinds.action,
    "action",
    Buffer.from([2, 2, 2, 2]),
    list(TestingHelper.argumentAbi("map", spec))
  );

  const contractAbi = new ContractAbi([], [fnAbiWithType], TestingHelper.namedTypeRef(0));

  const builder = new FnRpcBuilder("action", contractAbi);
  builder.addSizedByteArray(Buffer.from([1]));

  expect(BuilderHelper.builderToBytesBe(builder)).toEqual(bytesFromHex("0202020201"));
});

test("addSizedByteArrayWithoutType", () => {
  const builder = new FnRpcBuilder(Buffer.from([2, 2, 2, 2]));
  builder.addSizedByteArray(Buffer.from([1, 3]));

  expect(BuilderHelper.builderToBytesBe(builder)).toEqual(bytesFromHex("020202020103"));
});

test("sameShortname", () => {
  const fnAbiAction = TestingHelper.fnAbi(
    FnKinds.action,
    "action",
    Buffer.from([0]),
    list(TestingHelper.argumentAbi("valueType", TestingHelper.simpleTypeSpec(TypeIndex.u32)))
  );
  const fnAbiCallback = TestingHelper.fnAbi(
    FnKinds.callback,
    "callback",
    Buffer.from([0]),
    list(TestingHelper.argumentAbi("valueType", TestingHelper.simpleTypeSpec(TypeIndex.u8)))
  );
  const contractAbi = new ContractAbi(
    [],
    [fnAbiAction, fnAbiCallback],
    TestingHelper.namedTypeRef(0)
  );
  const builderAction = new FnRpcBuilder(Buffer.from([0]), contractAbi, FnKinds.action).addU32(1);
  const builderCallback = new FnRpcBuilder(Buffer.from([0]), contractAbi, FnKinds.callback).addU8(
    1
  );
  expect(BuilderHelper.builderToBytesBe(builderAction)).toEqual(bytesFromHex("0000000001"));
  expect(BuilderHelper.builderToBytesBe(builderCallback)).toEqual(bytesFromHex("0001"));
});

test("cantFindFunction", () => {
  const fnAbiCallback = TestingHelper.fnAbi(
    FnKinds.callback,
    "callback",
    Buffer.from([0]),
    list(TestingHelper.argumentAbi("valueType", TestingHelper.simpleTypeSpec(TypeIndex.u8)))
  );
  const contractAbi = new ContractAbi([], [fnAbiCallback], TestingHelper.namedTypeRef(0));
  expect(() => new FnRpcBuilder("action", contractAbi)).toThrowError();
  expect(() => new FnRpcBuilder(Buffer.from([0]), contractAbi, FnKinds.action)).toThrowError();
});

test("zkExtraByte", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-secret-voting/-/blob/main/src/lib.rs
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_secret_voting.abi",
    "start_vote_counting"
  );
  const bytes = BuilderHelper.builderToBytesBe(builder);
  expect(bytes).toEqual(bytesFromHex("0901"));
});

test("zkNotAction", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-secret-voting/-/blob/main/src/lib.rs
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_secret_voting.abi",
    "add_vote"
  );
  const bytes = BuilderHelper.builderToBytesBe(builder);
  expect(bytes).toEqual(bytesFromHex("40"));
});
