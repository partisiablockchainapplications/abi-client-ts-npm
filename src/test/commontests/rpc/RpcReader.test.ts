/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("readBool", () => {
  const rpcValue = RpcReaderHelper.wrap(
    "dddddd0d01",
    TestingHelper.simpleTypeSpec(TypeIndex.bool)
  ).readRpc();
  expect(rpcValue.fnAbi.name).toEqual("test");
  const argSize = rpcValue.arguments.length;
  expect(argSize).toEqual(rpcValue.size()); // commontests-ignore-array
  expect(rpcValue.get(0).boolValue()).toBeTruthy(); // commontests-ignore-array
});

test("readNumbers", () => {
  const rpcValueU8 = RpcReaderHelper.wrap(
    "dddddd0d02",
    TestingHelper.simpleTypeSpec(TypeIndex.u8)
  ).readRpc();
  expect(rpcValueU8.get(0).asNumber()).toEqual(2); // commontests-ignore-array

  const rpcValueI8 = RpcReaderHelper.wrap(
    "dddddd0dff",
    TestingHelper.simpleTypeSpec(TypeIndex.i8)
  ).readRpc();
  expect(rpcValueI8.get(0).asNumber()).toEqual(-1); // commontests-ignore-array

  const rpcValueU16 = RpcReaderHelper.wrap(
    "dddddd0d0201",
    TestingHelper.simpleTypeSpec(TypeIndex.u16)
  ).readRpc();
  expect(rpcValueU16.get(0).asNumber()).toEqual(0x0201); // commontests-ignore-array

  const rpcValueI16 = RpcReaderHelper.wrap(
    "dddddd0dffff",
    TestingHelper.simpleTypeSpec(TypeIndex.i16)
  ).readRpc();
  expect(rpcValueI16.get(0).asNumber()).toEqual(-1); // commontests-ignore-array

  const rpcValueU32 = RpcReaderHelper.wrap(
    "dddddd0d04030201",
    TestingHelper.simpleTypeSpec(TypeIndex.u32)
  ).readRpc();
  expect(rpcValueU32.get(0).asBN().eq(new BN("04030201", "hex"))).toBeTruthy(); // commontests-ignore-array

  const rpcValueI32 = RpcReaderHelper.wrap(
    "dddddd0dffffffff",
    TestingHelper.simpleTypeSpec(TypeIndex.i32)
  ).readRpc();
  expect(rpcValueI32.get(0).asNumber()).toEqual(-1); // commontests-ignore-array

  const rpcValueU64 = RpcReaderHelper.wrap(
    "dddddd0d0807060504030201",
    TestingHelper.simpleTypeSpec(TypeIndex.u64)
  ).readRpc();
  expect(
    rpcValueU64
      .get(0) // commontests-ignore-array
      .asBN()
      .eq(new BN("0807060504030201", 16))
  ).toBeTruthy();

  const rpcValueI64 = RpcReaderHelper.wrap(
    "dddddd0dffffffffffffffff",
    TestingHelper.simpleTypeSpec(TypeIndex.i64)
  ).readRpc();
  expect(rpcValueI64.get(0).asBN().eq(new BN("-1"))).toBeTruthy(); // commontests-ignore-array

  const rpcValueU128 = RpcReaderHelper.wrap(
    "dddddd0d08070605040302010807060504030201",
    TestingHelper.simpleTypeSpec(TypeIndex.u128)
  ).readRpc();
  expect(
    rpcValueU128
      .get(0) // commontests-ignore-array
      .asBN()
      .eq(new BN("08070605040302010807060504030201", 16))
  ).toBeTruthy();

  const rpcValueI128 = RpcReaderHelper.wrap(
    "dddddd0dffffffffffffffffffffffffffffffff",
    TestingHelper.simpleTypeSpec(TypeIndex.i128)
  ).readRpc();
  expect(
    rpcValueI128
      .get(0) // commontests-ignore-array
      .asBN()
      .eq(new BN("-1"))
  ).toBeTruthy();
});

test("readAddress", () => {
  const address = "010000000000000000000000000000000000000003";
  const rpcVal = RpcReaderHelper.wrap(
    "dddddd0d" + address,
    TestingHelper.simpleTypeSpec(TypeIndex.Address)
  ).readRpc();
  expect(rpcVal.get(0).addressValue().value) // commontests-ignore-array
    .toEqual(bytesFromHex(address));
});

test("readString", () => {
  const rpcVal = RpcReaderHelper.wrap(
    "dddddd0d0000000568656c6c6f",
    TestingHelper.simpleTypeSpec(TypeIndex.String)
  ).readRpc();
  expect(rpcVal.get(0).stringValue()).toEqual("hello"); // commontests-ignore-array
});

test("readSizedByteArray", () => {
  const rpcVal = RpcReaderHelper.wrap(
    "dddddd0d68656c6c6f",
    TestingHelper.sizedByteArrayTypeSpec(5)
  ).readRpc();
  expect(rpcVal.get(0).sizedByteArrayValue()) // commontests-ignore-array
    .toEqual(bytesFromHex("68656c6c6f"));
});

test("readOptionSome", () => {
  const rpcVal = RpcReaderHelper.wrap(
    "dddddd0d010000000568656c6c6f",
    TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.String))
  ).readRpc();
  const optionValue = rpcVal.get(0).optionValue(); // commontests-ignore-array
  expect(optionValue.isSome()).toBeTruthy();
  expect(requireNonNull(optionValue.innerValue).stringValue()).toEqual("hello");
});

test("readOptionNone", () => {
  const rpcVal = RpcReaderHelper.wrap(
    "dddddd0d00",
    TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.String))
  ).readRpc();
  expect(rpcVal.get(0).optionValue().isSome()).toBeFalsy(); // commontests-ignore-array
  // Here it is also null in TS, but Sed changes isNull to toBeUndefined as that is used other
  // places
  expect(rpcVal.get(0).optionValue().innerValue) // commontests-ignore-array
    .toBeNull(); // commontests-expect-null
});

test("readVector", () => {
  const rpcVal = RpcReaderHelper.wrap(
    "dddddd0d00000003000000010000000200000003",
    TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.i32))
  ).readRpc();
  const vecVal = rpcVal.get(0).vecValue(); // commontests-ignore-array
  expect(vecVal.size()).toEqual(3); // commontests-ignore-array
  expect(vecVal.get(0).asNumber()).toEqual(1); // commontests-ignore-array
  expect(vecVal.get(1).asNumber()).toEqual(2); // commontests-ignore-array
  expect(vecVal.get(2).asNumber()).toEqual(3); // commontests-ignore-array
});

test("readVecU8", () => {
  const rpcVal = RpcReaderHelper.wrap(
    "dddddd0d00000003010203",
    TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u8))
  ).readRpc();
  const vecVal = rpcVal.get(0).vecValue(); // commontests-ignore-array
  expect(vecVal.size()).toEqual(3); // commontests-ignore-array
  expect(vecVal.get(0).asNumber()).toEqual(1); // commontests-ignore-array
  expect(vecVal.get(1).asNumber()).toEqual(2); // commontests-ignore-array
  expect(vecVal.get(2).asNumber()).toEqual(3); // commontests-ignore-array
});

test("readStruct", () => {
  const contract = new ContractAbi(
    list(
      new StructTypeSpec(
        "MyStruct",
        list(
          TestingHelper.fieldAbi("f1", TestingHelper.simpleTypeSpec(TypeIndex.i8)),
          TestingHelper.fieldAbi("f2", TestingHelper.simpleTypeSpec(TypeIndex.i32))
        )
      )
    ),
    list(
      TestingHelper.fnAbi(
        FnKinds.action,
        "test",
        bytesFromHex("dddddd0d"),
        list(TestingHelper.argumentAbi("valueType", TestingHelper.namedTypeRef(0)))
      )
    ),
    TestingHelper.namedTypeRef(0)
  );
  const version = { major: 4, minor: 0, patch: 0 };
  const format = new Configuration(
    ShortnameType.leb128,
    FunctionFormat.FnKind,
    -1,
    NamedTypesFormat.OnlyStructs
  );
  const fileAbi = TestingHelper.fileAbi("PBCABI", version, version, format, contract);

  const rpcVal = RpcReaderHelper.wrap("dddddd0d1400000020", fileAbi).readRpc();
  const structVal = rpcVal.get(0).structValue(); // commontests-ignore-array
  expect(requireNonNull(structVal.getFieldValue("f1")).asNumber()).toEqual(20);
  expect(requireNonNull(structVal.getFieldValue("f2")).asNumber()).toEqual(32);
});

test("multipleArguments", () => {
  const contract = new ContractAbi(
    [],
    list(
      TestingHelper.fnAbi(
        FnKinds.action,
        "test",
        bytesFromHex("dddddd0d"),
        list(
          TestingHelper.argumentAbi("value1", TestingHelper.simpleTypeSpec(TypeIndex.i32)),
          TestingHelper.argumentAbi(
            "value2",
            TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.i8))
          ),
          TestingHelper.argumentAbi("value3", TestingHelper.simpleTypeSpec(TypeIndex.bool))
        )
      )
    ),
    TestingHelper.namedTypeRef(0)
  );
  const version = { major: 4, minor: 0, patch: 0 };
  const format = new Configuration(
    ShortnameType.leb128,
    FunctionFormat.FnKind,
    -1,
    NamedTypesFormat.OnlyStructs
  );
  const fileAbi = TestingHelper.fileAbi("PBCABI", version, version, format, contract);

  const rpcVal = RpcReaderHelper.wrap("dddddd0d00000014000000040102030400", fileAbi).readRpc();
  expect(rpcVal.size()).toEqual(3); // commontests-ignore-array
  expect(rpcVal.get(0).asNumber()).toEqual(20); // commontests-ignore-array
  expect(rpcVal.get(1).vecValue().get(2).asNumber()).toEqual(3); // commontests-ignore-array
  expect(rpcVal.get(2).boolValue()).toBeFalsy(); // commontests-ignore-array
});

test("invalidLebShortname", () => {
  const rpcReader = RpcReaderHelper.wrap(
    "dddddddddd0d01",
    TestingHelper.simpleTypeSpec(TypeIndex.bool)
  );
  expect(() => rpcReader.readRpc()).toThrowError(
    "Invalid LEB128 sequence, RPC header must be a valid 32-bit LEB128 encoded int (max 5" +
      " bytes)"
  );
});

test("noShortname", () => {
  const rpcReader = RpcReaderHelper.wrap(
    "eeeeee0e01",
    TestingHelper.simpleTypeSpec(TypeIndex.bool)
  );
  expect(() => rpcReader.readRpc()).toThrowError("No action with shortname eeeeee0e");
});

test("shortnameTypeHash", () => {
  const contract = new ContractAbi(
    [],
    list(
      TestingHelper.fnAbi(
        FnKinds.action,
        "test",
        bytesFromHex("7777dddddd0d"),
        list(TestingHelper.argumentAbi("valueType", TestingHelper.simpleTypeSpec(TypeIndex.bool)))
      )
    ),
    TestingHelper.namedTypeRef(0)
  );
  const version = { major: 1, minor: 0, patch: 0 };
  const format = new Configuration(
    ShortnameType.hash,
    FunctionFormat.InitSeparately,
    6,
    NamedTypesFormat.OnlyStructs
  );
  const fileAbi = TestingHelper.fileAbi("PBCABI", version, version, format, contract);
  const rpcVal = RpcReaderHelper.wrap("7777dddddd0d01", fileAbi).readRpc();
  expect(rpcVal.get(0).boolValue()).toBeTruthy(); // commontests-ignore-array
});

test("readSet", () => {
  const rpc = RpcReaderHelper.wrap(
    "dddddd0d00000003000000010000000200000003",
    TestingHelper.setTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u32))
  );
  expect(() => rpc.readRpc()).toThrowError("Type Set is not supported in rpc");
});

test("readMap", () => {
  const rpc = RpcReaderHelper.wrap(
    "dddddd0d00000003010000000102000000020300000003",
    TestingHelper.mapTypeSpec(
      TestingHelper.simpleTypeSpec(TypeIndex.u8),
      TestingHelper.simpleTypeSpec(TypeIndex.u32)
    )
  );
  expect(() => rpc.readRpc()).toThrowError("Type Map is not supported in rpc");
});

test("readEnum", () => {
  const namedTypes = list<NamedTypeSpec>(
    new StructTypeSpec(
      "MyStruct",
      list(
        TestingHelper.fieldAbi("f1", TestingHelper.simpleTypeSpec(TypeIndex.i8)),
        TestingHelper.fieldAbi("f2", TestingHelper.simpleTypeSpec(TypeIndex.i32))
      )
    ),
    new EnumTypeSpec("MyEnum", list(TestingHelper.enumVariant(0, TestingHelper.namedTypeRef(0))))
  );
  const contract = new ContractAbi(
    namedTypes,
    list(
      TestingHelper.fnAbi(
        FnKinds.action,
        "test",
        bytesFromHex("dddddd0d"),
        list(TestingHelper.argumentAbi("valueType", TestingHelper.namedTypeRef(1)))
      )
    ),
    TestingHelper.namedTypeRef(0)
  );
  const version = { major: 5, minor: 0, patch: 0 };
  const format = new Configuration(
    ShortnameType.leb128,
    FunctionFormat.FnKind,
    -1,
    NamedTypesFormat.StructsAndEnum
  );
  const fileAbi = TestingHelper.fileAbi("PBCABI", version, version, format, contract);

  const rpcVal = RpcReaderHelper.wrap("dddddd0d001400000020", fileAbi).readRpc();
  const enumVal = rpcVal.get(0).enumValue(); // commontests-ignore-array
  const structVal = enumVal.item;
  expect(enumVal.name).toEqual("MyStruct");
  expect(requireNonNull(structVal.getFieldValue("f1")).asNumber()).toEqual(20);
  expect(requireNonNull(structVal.getFieldValue("f2")).asNumber()).toEqual(32);

  const rpcReaderUndefinedEnum = RpcReaderHelper.wrap("dddddd0d011400000020", fileAbi);
  expect(() => rpcReaderUndefinedEnum.readRpc()).toThrowError(
    "Undefined EnumVariant 1 used in MyEnum"
  );
});

test("streamInput", () => {
  const contract = new ContractAbi(
    [],
    list(TestingHelper.fnAbi(FnKinds.action, "test", bytesFromHex("00"), [])),
    TestingHelper.namedTypeRef(0)
  );
  const version = { major: 4, minor: 1, patch: 0 };
  const format = new Configuration(
    ShortnameType.leb128,
    FunctionFormat.FnKind,
    -1,
    NamedTypesFormat.OnlyStructs
  );
  const fileAbi = TestingHelper.fileAbi("PBCABI", version, version, format, contract);
  const bytes = new BigEndianByteInput(Buffer.from([0]));
  const rpc = new RpcReader(bytes, fileAbi, FnKinds.action).readRpc();
  expect(rpc.fnAbi.name).toEqual("test");
});

test("zkExtraByte", () => {
  const abi = TestingHelper.loadAbiParserFromFile("contract_secret_voting.abi").parseAbi();
  const rpc = RpcReaderHelper.wrap("0901", abi).readRpc();
  expect(rpc.fnAbi.name).toEqual("start_vote_counting");
  const rpcError = RpcReaderHelper.wrap("0801", abi);
  expect(() => rpcError.readRpc()).toThrowError("First byte in RPC stream should be 9, but was 8");
});

test("zkNotAction", () => {
  const abi = TestingHelper.loadAbiParserFromFile("contract_secret_voting.abi").parseAbi();
  const rpc = new RpcReader(bytesFromHex("40"), abi, FnKinds.zkSecretInput).readRpc();
  expect(rpc.fnAbi.name).toEqual("add_vote");
});
