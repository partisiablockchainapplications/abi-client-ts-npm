/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("contractOption", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-options
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_options_v3.abi",
    "update_u64"
  );
  builder.addOption();
  const rpc = BuilderHelper.builderToBytesBe(builder);
  const expected = concatBytes(bytesFromHex("cf9cffe90b"), Buffer.from([0x00]));
  expect(rpc).toEqual(expected);
  const builderWithOption = TestingHelper.createBuilderFromFile(
    "contract_options_v3.abi",
    "update_string"
  );
  builderWithOption.addOption().addString("hello");

  const hasOptionRpc = BuilderHelper.builderToBytesBe(builderWithOption);
  const hasOptionExpected = concatBytes(
    bytesFromHex("f3eae9b808"),
    Buffer.from([0x01]),
    bytesFromStringBe("hello")
  );
  expect(hasOptionRpc).toEqual(hasOptionExpected);

  // Test without type checking
  const builderNoTypeCheck: FnRpcBuilder = new FnRpcBuilder(bytesFromHex("f3eae9b808"));
  builderNoTypeCheck.addOption().addString("hello");
  const rpcNoTypeCheck = BuilderHelper.builderToBytesBe(builderWithOption);
  expect(rpcNoTypeCheck).toEqual(hasOptionExpected);

  const builderNoTypeCheckNoOption: FnRpcBuilder = new FnRpcBuilder(bytesFromHex("cf9cffe90b"));
  builderNoTypeCheckNoOption.addOption();
  const test = BuilderHelper.builderToBytesBe(builderNoTypeCheckNoOption);
  expect(test).toEqual(expected);
});

test("noTypeCheckRpc", () => {
  const builder: FnRpcBuilder = new FnRpcBuilder(Buffer.alloc(4));
  builder.addOption().addU8(0x0a);
  builder.addOption();
  const rpc = BuilderHelper.builderToBytesBe(builder);
  const expected = bytesFromHex("00000000010A00");
  expect(rpc).toEqual(expected);

  const builderWithTwoOptions: FnRpcBuilder = new FnRpcBuilder(Buffer.alloc(4));
  builderWithTwoOptions.addOption().addOption().addI8(-1);
  const rpcTwoOptions = BuilderHelper.builderToBytesBe(builderWithTwoOptions);
  const expectedTwoOptions = bytesFromHex("000000000101ff");
  expect(rpcTwoOptions).toEqual(expectedTwoOptions);
});

test("doubleAdd", () => {
  const builder: FnRpcBuilder = new FnRpcBuilder(Buffer.alloc(4));
  expect(() => builder.addOption().addU8(0x0a).addU32(1)).toThrowError(
    "Cannot set option value twice."
  );
});

test("invalidType", () => {
  const initShortname = bytesFromHex("fffffffff0");
  const abi: FnAbi = TestingHelper.fnAbi(
    FnKinds.init,
    "init",
    initShortname,
    list(TestingHelper.argumentAbi("int64", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );

  const simpleContractAbi = new ContractAbi([], [abi], TestingHelper.namedTypeRef(0));
  const builder = new FnRpcBuilder("init", simpleContractAbi);
  expect(() => builder.addBool(true)).toThrowError(
    "In init/int64, Expected type u64, but got bool"
  );
});

test("typeCheck", () => {
  const initShortname = bytesFromHex("fffffffff0");
  const abi: FnAbi = TestingHelper.fnAbi(
    FnKinds.init,
    "init",
    initShortname,
    list(
      TestingHelper.argumentAbi(
        "option",
        TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
      )
    )
  );

  const contract = new ContractAbi([], [abi], TestingHelper.namedTypeRef(0));

  expect(() => new FnRpcBuilder("init", contract).addOption().addBool(true)).toThrowError(
    "In init/option, Expected type u64, but got bool"
  );

  expect(() => new FnRpcBuilder("init", contract).addOption().addOption()).toThrowError(
    "In init/option, Expected type u64, but got Option"
  );
});
