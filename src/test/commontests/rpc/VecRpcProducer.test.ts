/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("getAndSize", () => {
  const spec = TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64));
  const producer = new VecProducer<BigEndianByteOutput>(spec, null, "");

  expect(producer.size()).toEqual(0); // commontests-ignore-array

  producer.addU64(42);
  expect(producer.size()).toEqual(1); // commontests-ignore-array

  producer.addU64(43);
  expect(producer.size()).toEqual(2); // commontests-ignore-array

  const bytes0 = BuilderHelper.builderToBytesBe(producer.get(0)); // commontests-ignore-array
  const bytes1 = BuilderHelper.builderToBytesBe(producer.get(1)); // commontests-ignore-array
  expect(bytes0).toEqual(Buffer.from([0, 0, 0, 0, 0, 0, 0, 42]));
  expect(bytes1).toEqual(Buffer.from([0, 0, 0, 0, 0, 0, 0, 43]));
});

test("assertTypeErrorInStruct", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_simple_struct_v3.abi",
    "update_my_struct"
  );
  expect(() => builder.addStruct().addU64(2).addVec().addString("valueType")).toThrowError(
    "In update_my_struct/value/some_vector/0, Expected type u64, but got String"
  );

  const noVecBuilder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_simple_struct_v3.abi",
    "update_my_u64_using_struct"
  );
  expect(() => noVecBuilder.addVec()).toThrowError(
    "In update_my_u64_using_struct/value, Expected type Named, but got Vec"
  );
});

test("testVec", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-vector
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_simple_vector_v3.abi",
    "update_my_vec"
  );
  builder.addVec().addU64(1).addU64(2).addU64(3);
  const rpc = BuilderHelper.builderToBytesBe(builder);
  const expected = concatBytes(
    bytesFromHex("9d99e6e401"),
    bytesFromHex("00000003"),
    bytesFromHex("0000000000000001"),
    bytesFromHex("0000000000000002"),
    bytesFromHex("0000000000000003")
  );
  expect(rpc).toEqual(expected);

  // Testing with no type checking
  const noTypeCheckBuilder: FnRpcBuilder = new FnRpcBuilder(bytesFromHex("9d99e6e401"));
  noTypeCheckBuilder.addVec().addU64(1).addU64(2).addU64(3);
  expect(BuilderHelper.builderToBytesBe(noTypeCheckBuilder)).toEqual(expected);
});
