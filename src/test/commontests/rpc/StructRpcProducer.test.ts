/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("tooFew", () => {
  const structAbi: StructTypeSpec = new StructTypeSpec(
    "name",
    list(TestingHelper.fieldAbi("f1", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );

  const structType: NamedTypeRef = TestingHelper.namedTypeRef(0);
  const contractAbi: ContractAbi = new ContractAbi([structAbi], [], structType);
  const producer = new StructProducer<BigEndianByteOutput>(contractAbi, "", structType);

  const out = new BigEndianByteOutput();
  expect(() => producer.write(out)).toThrowError("Missing argument 'f1'");
});

test("tooManyElements", () => {
  const structAbi: StructTypeSpec = new StructTypeSpec(
    "name",
    list(TestingHelper.fieldAbi("f1", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );
  const structType: NamedTypeRef = TestingHelper.namedTypeRef(0);
  const contractAbi: ContractAbi = new ContractAbi([structAbi], [], structType);
  const producer = new StructProducer<BigEndianByteOutput>(contractAbi, "", structType);
  producer.addU64(1);

  expect(() => producer.addU64(1)).toThrowError(
    "Cannot add more arguments than the struct has fields."
  );
});

test("testSimpleStruct", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_simple_struct_v3.abi",
    "update_my_struct"
  );
  builder.addStruct().addU64(2).addVec().addU64(1).addU64(1);

  // Encoding the element: 2
  const expectedForU64 = bytesFromHex("0000000000000002");
  // 2 arguments (4 bytes) of u64 (8 bytes) each has valueType 01
  const expectedForVec = bytesFromHex("00000002" + "0000000000000001" + "0000000000000001");

  const rpc = BuilderHelper.builderToBytesBe(builder);
  const expected = concatBytes(bytesFromHex("f3b68ff40e"), expectedForU64, expectedForVec);
  expect(rpc).toEqual(expected);
});

test("assertTypeError", () => {
  // Contract can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_simple_struct_v3.abi",
    "update_my_struct"
  );
  expect(() => builder.addBool(false)).toThrowError(
    "In update_my_struct/value, Expected type Named, but got bool"
  );

  const noStructType: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_booleans_v3.abi",
    "update_my_bool"
  );
  expect(() => noStructType.addStruct()).toThrowError(
    "In update_my_bool/value, Expected type bool, but got Named"
  );
});

test("assertTypeErrorInStruct", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_simple_struct_v3.abi",
    "update_my_struct"
  );
  expect(() => builder.addStruct().addString("string")).toThrowError(
    "In update_my_struct/value/some_value, Expected type u64, but got String"
  );
});

test("structOfStruct", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_struct_of_struct_v3.abi",
    "update_my_other_struct"
  );
  builder.addStruct().addU64(1).addVec().addStruct().addU64(2).addVec().addU64(3);
  const rpc = BuilderHelper.builderToBytesBe(builder);
  const expected = concatBytes(
    bytesFromHex("85f292be0b"),
    bytesFromHex("0000000000000001"),
    // One element
    bytesFromHex(
      "00000001" +
        "0000000000000002" +
        // One element
        "00000001" +
        "0000000000000003"
    )
  );
  expect(rpc).toEqual(expected);

  // Testing without providing type checking
  const builderNoTypeCheck: FnRpcBuilder = new FnRpcBuilder(bytesFromHex("85f292be0b"));
  builderNoTypeCheck.addStruct().addU64(1).addVec().addStruct().addU64(2).addVec().addU64(3);

  const noTypeCheckRpc = BuilderHelper.builderToBytesBe(builderNoTypeCheck);
  expect(noTypeCheckRpc).toEqual(expected);
});

test("moreStructsInVec", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct
  const builder: FnRpcBuilder = TestingHelper.createBuilderFromFile(
    "contract_struct_of_struct_v3.abi",
    "update_my_other_struct"
  );
  const vecRpcProducer: VecProducer<BigEndianByteOutput> = builder
    .addStruct()
    .addU64(new BN("FFFFFFFFFFFFFFFF", "hex"))
    .addVec();
  vecRpcProducer.addStruct().addU64(1).addVec().addU64(2);
  vecRpcProducer.addStruct().addU64(3).addVec().addU64(4);

  const rpc = BuilderHelper.builderToBytesBe(builder);
  const expected = concatBytes(
    bytesFromHex("85f292be0b"),
    bytesFromHex("ffffffffffffffff"),
    // Two elements in Vec
    bytesFromHex("00000002"),
    // First is struct with 1, Vec<2>
    bytesFromHex("0000000000000001" + "00000001" + "0000000000000002"),
    // Second is struct with 3, <Vec<4>
    bytesFromHex("0000000000000003" + "00000001" + "0000000000000004")
  );
  expect(rpc).toEqual(expected);
});
