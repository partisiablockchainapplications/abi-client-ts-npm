/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("assertErrorsForTypes", () => {
  const input = new LittleEndianByteInput(bytesFromHex("0100000042" + "00"));
  const contract: ContractAbi = new ContractAbi([], [], TestingHelper.namedTypeRef(0));

  const reader: StateReader = new StateReader(input, contract);

  const stringValue: ScValue = reader.readStateValue(
    TestingHelper.simpleTypeSpec(TypeIndex.String)
  );

  expect(() => stringValue.boolValue()).toThrowError("Cannot read bool for current type");
  expect(() => stringValue.setValue()).toThrowError("Cannot read Set for current type");
  expect(() => stringValue.vecValue()).toThrowError("Cannot read Vector for current type");
  expect(() => stringValue.optionValue()).toThrowError("Cannot read Option for current type");
  expect(() => stringValue.structValue()).toThrowError("Cannot read Struct for current type");
  expect(() => stringValue.addressValue()).toThrowError("Cannot read Address for current type");
  expect(() => stringValue.mapValue()).toThrowError("Cannot read Map for current type");
  expect(() => stringValue.sizedByteArrayValue()).toThrowError(
    "Cannot read SizedByteArray for current type"
  );
  expect(() => stringValue.enumValue()).toThrowError("Cannot read Enum for current type");

  const boolValue: ScValue = reader.readStateValue(TestingHelper.simpleTypeSpec(TypeIndex.bool));
  expect(() => boolValue.stringValue()).toThrowError("Cannot read String for current type");
});

test("assertErrorsForIntegerTypes", () => {
  const stringValue: ScValue = new ScValueString("hello");

  expect(() => stringValue.asNumber()).toThrowError();
  expect(() => stringValue.asBN()).toThrowError();
  expect(() => stringValue.asBN()).toThrowError();
});
