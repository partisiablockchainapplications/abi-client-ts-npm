/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("invalidAddress", () => {
  expect(() => new ScValueAddress(Buffer.alloc(2))).toThrowError(
    "Address expects exactly 21 bytes, but found 2"
  );
});

test("getType", () => {
  const address = new ScValueAddress(Buffer.alloc(21));
  expect(address.getType()).toEqual(TypeIndex.Address);

  const addressTest = new ScValueAddress(Buffer.alloc(21));
  expect(addressTest.getType()).toEqual(TypeIndex.Address);
});

test("value", () => {
  const rawValue = bytesFromHex("010000000000000000000000000000000000000020");

  const addressTest = new ScValueAddress(rawValue);
  expect(addressTest.addressValue().value).toEqual(rawValue);
});

test("equals", () => {
  const rawValue1 = bytesFromHex("010000000000000000000000000000000000000020");
  const rawValue2 = bytesFromHex("020000000000000000000000000000000000000020");

  const address1 = new ScValueAddress(rawValue1);
  const address2 = new ScValueAddress(rawValue2);

  expect(address1).not.toEqual(address2);
  expect(address1).not.toEqual(null);
  expect(address1).toEqual(address1);
  expect(address1).toEqual(new ScValueAddress(rawValue1));
  expect(address1).not.toEqual(address2);
  expect(address1).not.toEqual("");
});
