/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("rpcTest", () => {
  const contract = new ContractAbi(
    [],
    list(
      TestingHelper.fnAbi(
        FnKinds.action,
        "jsonTest",
        bytesFromHex("dddddd0d"),
        list(
          TestingHelper.argumentAbi("value1", TestingHelper.simpleTypeSpec(TypeIndex.bool)),
          TestingHelper.argumentAbi("value2", TestingHelper.simpleTypeSpec(TypeIndex.i32)),
          TestingHelper.argumentAbi(
            "value3",
            TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.i8))
          )
        )
      )
    ),
    TestingHelper.namedTypeRef(0)
  );
  const abiVersion = { major: 4, minor: 0, patch: 0 };
  const format = new Configuration(
    ShortnameType.leb128,
    FunctionFormat.FnKind,
    -1,
    NamedTypesFormat.OnlyStructs
  );
  const fileAbi = TestingHelper.fileAbi("PBCABI", abiVersion, abiVersion, format, contract);

  const rpcVal = new RpcReader(
    bytesFromHex("dddddd0d00000000140000000401020304"),
    fileAbi,
    FnKinds.action
  ).readRpc();
  ValueHelper.assertRpcJson(
    rpcVal,
    "{\n" +
      '  "actionName" : "jsonTest",\n' +
      '  "arguments" : {\n' +
      '    "value1" : false,\n' +
      '    "value2" : 20,\n' +
      '    "value3" : [ 1, 2, 3, 4 ]\n' +
      "  }\n" +
      "}"
  );
});
