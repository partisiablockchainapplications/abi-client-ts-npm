/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { Producer } from "../../main";
import { BigEndianByteOutput, LittleEndianByteOutput } from "bitmanipulation-ts";

export const BuilderHelper = {
  builderToBytesBe(rpc: Producer<BigEndianByteOutput>): Buffer {
    const bufferWriter = new BigEndianByteOutput();
    rpc.write(bufferWriter);
    return bufferWriter.toBuffer();
  },

  builderToBytesLe(rpc: Producer<LittleEndianByteOutput>): Buffer {
    const bufferWriter = new LittleEndianByteOutput();
    rpc.write(bufferWriter);
    return bufferWriter.toBuffer();
  },
};
