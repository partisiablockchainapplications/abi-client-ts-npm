/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("buildEveryThing", () => {
  const mapType = TestingHelper.mapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.String),
    TestingHelper.simpleTypeSpec(TypeIndex.u64)
  );

  const mapEntryType: StructTypeSpec = new StructTypeSpec(
    "MapEntry$String$u64",
    list(
      TestingHelper.fieldAbi("keyType", mapType.keyType),
      TestingHelper.fieldAbi("valueType", mapType.valueType)
    )
  );

  const fieldAbiList = list(
    TestingHelper.fieldAbi("field1", TestingHelper.simpleTypeSpec(TypeIndex.i8)),
    TestingHelper.fieldAbi("field2", TestingHelper.simpleTypeSpec(TypeIndex.u8)),
    TestingHelper.fieldAbi("field3", TestingHelper.simpleTypeSpec(TypeIndex.i16)),
    TestingHelper.fieldAbi("field4", TestingHelper.simpleTypeSpec(TypeIndex.u16)),
    TestingHelper.fieldAbi("field5", TestingHelper.simpleTypeSpec(TypeIndex.i32)),
    TestingHelper.fieldAbi("field6", TestingHelper.simpleTypeSpec(TypeIndex.u32)),
    TestingHelper.fieldAbi("field7", TestingHelper.simpleTypeSpec(TypeIndex.i64)),
    TestingHelper.fieldAbi("field8", TestingHelper.simpleTypeSpec(TypeIndex.u64)),
    TestingHelper.fieldAbi("field9", TestingHelper.simpleTypeSpec(TypeIndex.i128)),
    TestingHelper.fieldAbi("field10", TestingHelper.simpleTypeSpec(TypeIndex.u128)),
    TestingHelper.fieldAbi("field11", TestingHelper.simpleTypeSpec(TypeIndex.String)),
    TestingHelper.fieldAbi("field12", TestingHelper.simpleTypeSpec(TypeIndex.bool)),
    TestingHelper.fieldAbi("field13", TestingHelper.simpleTypeSpec(TypeIndex.bool)),
    TestingHelper.fieldAbi(
      "field14",
      TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.i64))
    ),
    TestingHelper.fieldAbi("field15", TestingHelper.namedTypeRef(0)),
    TestingHelper.fieldAbi(
      "field16",
      TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.i64))
    ),
    TestingHelper.fieldAbi(
      "field17",
      TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
    ),
    TestingHelper.fieldAbi("field18", mapType),
    TestingHelper.fieldAbi("field19", TestingHelper.namedTypeRef(3)),
    TestingHelper.fieldAbi("field20", TestingHelper.namedTypeRef(3))
  );

  const structOfU64 = new StructTypeSpec(
    "",
    list(TestingHelper.fieldAbi("field", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );
  const stateStruct = new StructTypeSpec("name", fieldAbiList);

  const structRef = TestingHelper.namedTypeRef(1);

  const enumStruct1 = new StructTypeSpec(
    "enumVariant1",
    list(TestingHelper.fieldAbi("field", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );
  const enumStruct2 = new StructTypeSpec("enumVariant2", []);

  const enumType = new EnumTypeSpec(
    "myEnum",
    list(
      TestingHelper.enumVariant(1, TestingHelper.namedTypeRef(4)),
      TestingHelper.enumVariant(2, TestingHelper.namedTypeRef(5))
    )
  );

  const stateProducer = new StructProducer<LittleEndianByteOutput>(
    new ContractAbi(
      [structOfU64, stateStruct, mapEntryType, enumType, enumStruct1, enumStruct2],
      [],
      structRef
    ),
    ""
  );

  const fn = (b: AbstractBuilder<LittleEndianByteOutput>) => {
    const builder: AbstractBuilder<LittleEndianByteOutput> = b
      .addI8(-128)
      .addU8(127)
      .addI16(-32768)
      .addU16(32767)
      .addI32(-2147483648)
      .addU32(2147483647)
      .addI64(new BN("-9223372036854775808"))
      .addU64(new BN("9223372036854775807"))
      .addI128(new BN("-170141183460469231731687303715884105728"))
      .addU128(new BN("170141183460469231731687303715884105727"))
      .addString("Test string")
      .addBool(true)
      .addBool(false);
    builder.addOption().addI64(42);
    builder.addStruct().addU64(1);
    builder.addVec().addI64(2);
    builder.addSet().addU64(3);
    builder.addMap().addString("key").addU64(4);
    builder.addEnumVariant(1).addU64(2);
    builder.addEnumVariant(2);
  };

  fn(stateProducer);

  const actual = BuilderHelper.builderToBytesLe(stateProducer);

  const expected = bytesFromHex(
    "" +
      "80" + // Hex format of Byte.MIN_VALUE
      "7f" + // Hex format of Byte.MAX_VALUE
      "0080" + // Hex format of Short.MIN_VALUE
      "ff7f" + // Hex format of Short.MAX_VALUE
      "00000080" + // Hex format of Integer.MIN_VALUE
      "ffffff7f" + // Hex format of Integer.MAX_VALUE
      "0000000000000080" + // Hex format of Long.MIN_VALUE
      "ffffffffffffff7f" + // Hex format of Long.MAX_VALUE
      "00000000000000000000000000000080" + // Hex format of
      // BigEndianDataOutput.MIN_128_BIT_VALUE
      "ffffffffffffffffffffffffffffff7f" + // Hex format of
      // BigEndianDataOutput.MAX_128_BIT_VALUE
      "0b0000005465737420737472696e67" + // Hex format of String "Test string"
      "01" + // Hex format of Boolean True valueType
      "00" + // Hex format of Boolean False Value
      "01" + // Hex format of an Option
      "2a00000000000000" + // Hex format of the option's u64 valueType of 42
      "0100000000000000" + // Hex format of the struct's u64 valueType of 1
      "01000000" + // Hex format of a vector of one entry
      "0200000000000000" + // Hex format of the vector's u64 valueType of 2
      "01000000" + // Hex format of set of one entry
      "0300000000000000" + // Hex format of the set's u64 valueType of 3
      "01000000" + // Hex format of a map of one entry
      "030000006b6579" + // Hex format of the keyType of the map's entry
      "0400000000000000" + // Hex format of the valueType of the map's entry
      "01" + // Hex format of the byte for the Enum variants discriminant
      "0200000000000000" + // Hex format of the enum variant's u64 valueType of 2
      "02"
  ); // Hex format of the byte for the Enum variants discriminant

  expect(actual).toEqual(expected);
});

test("simpleBoolTest", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
  const contract: ContractAbi = TestingHelper.getContractAbiFromFile("contract_booleans_v3.abi");

  const state = BuilderHelper.builderToBytesLe(
    new StructProducer<LittleEndianByteOutput>(contract, "").addBool(false)
  );
  const expected = Buffer.from([0x00]);
  expect(state).toEqual(expected);

  const stateProducerTrue: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contract,
    ""
  );
  const stateTrue = BuilderHelper.builderToBytesLe(stateProducerTrue.addBool(true));
  const expectedTrue = Buffer.from([0x01]);
  expect(stateTrue).toEqual(expectedTrue);
});

test("contractAddress", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-address
  const address = "b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff";
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_address_v3.abi");
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  const state = BuilderHelper.builderToBytesLe(stateProducer.addAddress(bytesFromHex(address)));
  const expected = bytesFromHex(address);
  expect(state).toEqual(expected);

  const invalidProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contractAbi,
    ""
  );
  expect(() =>
    BuilderHelper.builderToBytesLe(invalidProducer.addAddress(Buffer.alloc(4)))
  ).toThrowError("Address must have length 21 bytes, got length = 4, value = 00000000");
});

test("contractEnum", () => {
  // Contract source can be found at:
  // https://gitlab.com/secata/pbc/language/contracts/testing-types/-/tree/main/contract-enum
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_enum.abi");
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  const state = BuilderHelper.builderToBytesLe(
    stateProducer.addEnumVariant(5).addU8(0x00).addBool(false)
  );
  const expected = bytesFromHex("050000");
  expect(state).toEqual(expected);

  const invalidProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contractAbi,
    ""
  );
  expect(() => BuilderHelper.builderToBytesLe(invalidProducer.addEnumVariant(9))).toThrowError(
    "In /my_enum, Undefined variant discriminant 9 for Vehicle"
  );
});

test("assertTypeError", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-address
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_address_v3.abi");
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  expect(() => BuilderHelper.builderToBytesLe(stateProducer.addBool(false))).toThrowError(
    "In /my_address, Expected type Address, but got bool"
  );
});

test("contractString", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-strings
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_strings_v3.abi");
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  const state = BuilderHelper.builderToBytesLe(stateProducer.addString("hello"));
  expect(state).toEqual(bytesFromStringLe("hello"));
});

test("canDelayGeneration", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-nested-vector
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_nested_vector_v3.abi"
  );
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  const outerVecStateProducer: VecProducer<LittleEndianByteOutput> = stateProducer.addVec();
  const innerVecStateProducer: VecProducer<LittleEndianByteOutput> = outerVecStateProducer.addVec();
  outerVecStateProducer.addVec().addU64(1);
  innerVecStateProducer.addU64(10).addU64(11);

  const state = BuilderHelper.builderToBytesLe(stateProducer);

  // We expect the following: [[10, 11]], [1]]
  const expected = concatBytes(
    // Two elements in outer vec
    bytesFromHex("02000000"),
    // First valueType has 10, 11
    bytesFromHex("02000000" + "0A000000000000000B00000000000000"),
    bytesFromHex("010000000100000000000000")
  );
  expect(state).toEqual(expected);
});

test("noTypeChecking", () => {
  const spec = TestingHelper.sizedByteArrayTypeSpec(4);
  const structAbi = new StructTypeSpec("name", list(TestingHelper.fieldAbi("f", spec)));
  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(
    simpleContractAbi,
    "",
    null
  );
  const state = BuilderHelper.builderToBytesLe(
    stateProducer.addBool(false).addU64(2).addString("hello")
  );
  const expected = bytesFromHex("0002000000000000000500000068656c6c6f");
  expect(state).toEqual(expected);
});

test("noTypeCheckingEnum", () => {
  const spec = TestingHelper.sizedByteArrayTypeSpec(4);
  const structType = new StructTypeSpec("enumVariant", list(TestingHelper.fieldAbi("f", spec)));
  const enumType = new EnumTypeSpec(
    "enum",
    list(TestingHelper.enumVariant(0, TestingHelper.namedTypeRef(0)))
  );
  const stateType = new StructTypeSpec(
    "state",
    list(TestingHelper.fieldAbi("my_enum", TestingHelper.namedTypeRef(1)))
  );

  const simpleContractAbi = new ContractAbi(
    [structType, enumType, stateType],
    [],
    TestingHelper.namedTypeRef(2)
  );
  const enumProducer: EnumVariantProducer<LittleEndianByteOutput> = new EnumVariantProducer(
    simpleContractAbi,
    "",
    null,
    0
  );
  const myEnum = BuilderHelper.builderToBytesLe(
    enumProducer.addBool(false).addU64(2).addString("hello")
  );
  const expected = bytesFromHex("000002000000000000000500000068656c6c6f");
  expect(myEnum).toEqual(expected);
  const nullContract = new EnumVariantProducer<LittleEndianByteOutput>(
    null,
    "",
    TestingHelper.namedTypeRef(0),
    0
  );
  const actual = BuilderHelper.builderToBytesLe(nullContract.addBool(true));
  expect(actual).toEqual(bytesFromHex("0001"));
});

test("tooFewArguments", () => {
  const structAbi = new StructTypeSpec(
    "name",
    list(TestingHelper.fieldAbi("f", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );
  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));

  expect(() =>
    BuilderHelper.builderToBytesLe(new StructProducer(simpleContractAbi, ""))
  ).toThrowError("Missing argument 'f'");
});

test("tooManyArguments", () => {
  const structAbi = new StructTypeSpec(
    "name",
    list(TestingHelper.fieldAbi("f", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );

  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));

  const stateProducer = new StructProducer<LittleEndianByteOutput>(simpleContractAbi, "");
  stateProducer.addU64(1);

  expect(() => stateProducer.addU8(0)).toThrowError(
    "Cannot add more arguments than the struct has fields."
  );
});

test("addSizedByteArrayWithType", () => {
  const spec = TestingHelper.sizedByteArrayTypeSpec(4);
  const structAbi = new StructTypeSpec("name", list(TestingHelper.fieldAbi("f", spec)));
  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));
  const structProducer = new StructProducer<LittleEndianByteOutput>(simpleContractAbi, "");
  structProducer.addSizedByteArray(Buffer.from([2, 2, 2, 2]));

  expect(BuilderHelper.builderToBytesLe(structProducer)).toEqual(bytesFromHex("02020202"));
});

test("addSizedByteArrayWithoutType", () => {
  const spec = TestingHelper.sizedByteArrayTypeSpec(4);
  const structAbi = new StructTypeSpec("name", list(TestingHelper.fieldAbi("f", spec)));
  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(
    simpleContractAbi,
    "",
    null
  );
  stateProducer.addSizedByteArray(Buffer.from([1, 3]));
  const state = BuilderHelper.builderToBytesLe(stateProducer);
  expect(state).toEqual(bytesFromHex("0103"));
});

test("mapState", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-map-of-struct
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_map_of_struct_v3.abi"
  );
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  const mapStateProducer: MapProducer<LittleEndianByteOutput> = stateProducer.addMap();
  mapStateProducer.addString("key1").addStruct().addU64(2).addVec().addU64(3);
  mapStateProducer.addString("key2").addStruct().addU64(4).addVec().addU64(6);
  const state = BuilderHelper.builderToBytesLe(stateProducer);
  const expected = concatBytes(
    bytesFromHex("02000000"),
    bytesFromStringLe("key1"),
    bytesFromHex("0200000000000000"),
    bytesFromHex("01000000"),
    bytesFromHex("0300000000000000"),
    bytesFromStringLe("key2"),
    bytesFromHex("0400000000000000"),
    bytesFromHex("01000000"),
    bytesFromHex("0600000000000000")
  );
  expect(state).toEqual(expected);
});
