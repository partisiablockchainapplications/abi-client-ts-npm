/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("size", () => {
  const spec = TestingHelper.mapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.String),
    TestingHelper.simpleTypeSpec(TypeIndex.u64)
  );
  const mapEntryType: StructTypeSpec = new StructTypeSpec(
    "MapEntry$String$u64",
    list(
      TestingHelper.fieldAbi("keyType", spec.keyType),
      TestingHelper.fieldAbi("valueType", spec.valueType)
    )
  );
  const contract = new ContractAbi([mapEntryType], [], TestingHelper.namedTypeRef(0));
  const producer = new MapProducer<LittleEndianByteOutput>(spec, contract, "");

  expect(producer.size()).toEqual(0); // commontests-ignore-array

  producer.addString("key1").addU64(1);
  expect(producer.size()).toEqual(1); // commontests-ignore-array

  producer.addString("key2").addU64(2);
  expect(producer.size()).toEqual(2); // commontests-ignore-array
});

test("assertTypeError", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-map
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_simple_map.abi");
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  expect(() => stateProducer.addBool(true)).toThrowError(
    "In /my_map, Expected type Map, but got bool"
  );

  const mapStateProducer: MapProducer<LittleEndianByteOutput> = stateProducer.addMap();
  expect(() => mapStateProducer.addMap().addStruct()).toThrowError(
    "In /my_map/0/key, Expected type Address, but got Map"
  );

  expect(() => mapStateProducer.addAddress(Buffer.alloc(21)).addStruct()).toThrowError(
    "In /my_map/0/value, Expected type u8, but got Named"
  );
});

test("testMap", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-map
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_simple_map.abi");
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  const mapStateProducer: MapProducer<LittleEndianByteOutput> = stateProducer.addMap();
  mapStateProducer.addAddress(bytesFromHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff")).addU8(0);
  mapStateProducer.addAddress(bytesFromHex("c002131a2b3c6741b42cfa4c33a2830602a3f2e9fd")).addU8(1);

  const state = BuilderHelper.builderToBytesLe(stateProducer);
  const expected = concatBytes(
    bytesFromHex("02000000"),
    bytesFromHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"),
    bytesFromHex("00"),
    bytesFromHex("c002131a2b3c6741b42cfa4c33a2830602a3f2e9fd"),
    bytesFromHex("01")
  );
  expect(state).toEqual(expected);

  // Testing with no type checking
  const producerNoTypeCheck: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contractAbi,
    "",
    null
  );
  const mapStateProducerNoTypeCheck: MapProducer<LittleEndianByteOutput> =
    producerNoTypeCheck.addMap();
  mapStateProducerNoTypeCheck
    .addAddress(bytesFromHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"))
    .addU8(0);
  mapStateProducerNoTypeCheck
    .addAddress(bytesFromHex("c002131a2b3c6741b42cfa4c33a2830602a3f2e9fd"))
    .addU8(1);
  const stateNoTypeCheck = BuilderHelper.builderToBytesLe(mapStateProducerNoTypeCheck);
  expect(stateNoTypeCheck).toEqual(expected);
});

test("missingValueWrite", () => {
  const spec = TestingHelper.mapTypeSpec(
    TestingHelper.simpleTypeSpec(TypeIndex.String),
    TestingHelper.simpleTypeSpec(TypeIndex.Address)
  );
  const mapEntryType: StructTypeSpec = new StructTypeSpec(
    "MapEntry$String$u64",
    list(
      TestingHelper.fieldAbi("keyType", spec.keyType),
      TestingHelper.fieldAbi("valueType", spec.valueType)
    )
  );
  const contract = new ContractAbi([mapEntryType], [], TestingHelper.namedTypeRef(0));
  const producer = new MapProducer<LittleEndianByteOutput>(spec, contract, "");
  producer.addString("k1");
  expect(() => producer.write(new LittleEndianByteOutput())).toThrowError("Missing value for key");
});
