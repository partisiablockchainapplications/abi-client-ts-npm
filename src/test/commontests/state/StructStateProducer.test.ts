/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("tooFew", () => {
  const structAbi = new StructTypeSpec(
    "name",
    list(TestingHelper.fieldAbi("f", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );

  const structRef: NamedTypeRef = TestingHelper.namedTypeRef(0);
  const contractAbi: ContractAbi = new ContractAbi([structAbi], [], structRef);
  const producer = new StructProducer<LittleEndianByteOutput>(contractAbi, "", structRef);

  const out = new LittleEndianByteOutput();
  expect(() => producer.write(out)).toThrowError("Missing argument 'f'");
});

test("tooManyElements", () => {
  const structAbi = new StructTypeSpec(
    "name",
    list(TestingHelper.fieldAbi("f", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );
  const structRef: NamedTypeRef = TestingHelper.namedTypeRef(0);
  const contractAbi: ContractAbi = new ContractAbi([structAbi], [], structRef);
  const producer = new StructProducer<LittleEndianByteOutput>(contractAbi, "", structRef);
  producer.addU64(1);

  expect(() => producer.addU64(1)).toThrowError(
    "Cannot add more arguments than the struct has fields."
  );
});

test("testSimpleStruct", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_simple_struct_v3.abi"
  );
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  stateProducer.addU64(2).addStruct().addU64(2).addVec().addU64(1).addU64(1);

  // Encoding the element: 2
  const expectedForU64 = bytesFromHex("0200000000000000");
  // 2 arguments (4 bytes) of u64 (8 bytes) each has valueType 01
  const expectedForVec = bytesFromHex("02000000" + "0100000000000000" + "0100000000000000");

  const state = BuilderHelper.builderToBytesLe(stateProducer);
  const expected = concatBytes(expectedForU64, expectedForU64, expectedForVec);
  expect(state).toEqual(expected);
});

test("assertTypeError", () => {
  // Contract can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_simple_struct_v3.abi"
  );
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  expect(() => stateProducer.addU64(1).addBool(false)).toThrowError(
    "In /my_struct, Expected type Named, but got bool"
  );

  // Contract can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-booleans
  const noStructTypeContractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_booleans_v3.abi"
  );
  const noStructType: StructProducer<LittleEndianByteOutput> = new StructProducer(
    noStructTypeContractAbi,
    ""
  );
  expect(() => noStructType.addStruct()).toThrowError(
    "In /my_bool, Expected type bool, but got Named"
  );
});

test("assertTypeErrorInStruct", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_simple_struct_v3.abi"
  );
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  expect(() => stateProducer.addU64(1).addStruct().addString("string")).toThrowError(
    "In /my_struct/some_value, Expected type u64, but got String"
  );
});

test("structOfStruct", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_struct_of_struct_v3.abi"
  );
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  stateProducer.addStruct().addU64(1).addVec().addStruct().addU64(2).addVec().addU64(3);
  const state = BuilderHelper.builderToBytesLe(stateProducer);
  const structInBytes = concatBytes(
    bytesFromHex("0100000000000000"),
    // One element
    bytesFromHex(
      "01000000" +
        "0200000000000000" +
        // One element
        "01000000" +
        "0300000000000000"
    )
  );
  expect(state).toEqual(structInBytes);
});

test("withoutProvidingTypeChecking", () => {
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_struct_of_struct_v3.abi"
  );
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contractAbi,
    "",
    null
  );
  stateProducer.addStruct().addU64(1).addVec().addStruct().addU64(2).addVec().addU64(3);
  const state = BuilderHelper.builderToBytesLe(stateProducer);
  const structInBytes = concatBytes(
    bytesFromHex("0100000000000000"),
    // One element
    bytesFromHex(
      "01000000" +
        "0200000000000000" +
        // One element
        "01000000" +
        "0300000000000000"
    )
  );
  expect(state).toEqual(structInBytes);
});

test("moreStructureInVec", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-struct-of-struct
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_struct_of_struct_v3.abi"
  );
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  const vecStateProducer: VecProducer<LittleEndianByteOutput> = stateProducer
    .addStruct()
    .addU64(new BN("FFFFFFFFFFFFFFFF", "hex"))
    .addVec();
  vecStateProducer.addStruct().addU64(1).addVec().addU64(2);
  vecStateProducer.addStruct().addU64(3).addVec().addU64(4);

  const state = BuilderHelper.builderToBytesLe(stateProducer);
  const expected = concatBytes(
    bytesFromHex("ffffffffffffffff"),
    // Two elements in Vec
    bytesFromHex("02000000"),
    // First is struct with 1, Vec<2>
    bytesFromHex("0100000000000000" + "01000000" + "0200000000000000"),
    // Second is struct with 3, <Vec<4>
    bytesFromHex("0300000000000000" + "01000000" + "0400000000000000")
  );
  expect(state).toEqual(expected);
});

test("structStateBuilderNullContract", () => {
  expect(() => new StructProducer<LittleEndianByteOutput>(null, "")).toThrowError();
});
