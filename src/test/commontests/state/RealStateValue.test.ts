/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("testTokenContract", () => {
  expect(StateReaderHelper.stringify("token_contract.abi", "token_contract.state")).toEqual(
    "#[state]\n" +
      "TokenContractState {\n" +
      '    name: "Token Name".to_string(),\n' +
      "    decimals: 18u8,\n" +
      '    symbol: "SYMB".to_string(),\n' +
      '    owner: "020000000000000000000000000000000000000001",\n' +
      "    total_supply: 123123123u64,\n" +
      "    balances: BTreeMap::from([\n" +
      '        ("020000000000000000000000000000000000000001", 123123123u64)\n' +
      "    ]),\n" +
      "    allowed: BTreeMap::from([\n" +
      "    ]),\n" +
      "}"
  );
});

test("testContractWithEnum", () => {
  // Contract source can be found at:
  // https://gitlab.com/secata/pbc/language/contracts/testing-types/-/tree/main/contract-enum
  expect(StateReaderHelper.stringify("contract_enum.abi", "contract_enum.state")).toEqual(
    "#[state]\n" +
      "EnumContractState {\n" +
      "    my_enum: Car {\n" +
      "        engine_size: 1u8,\n" +
      "        supports_trailer: false\n" +
      "    },\n" +
      "}"
  );
});

test("testTokenContractValues", () => {
  const parser: AbiParser = TestingHelper.loadAbiParserFromFile("token_contract.abi");
  const contract = parser.parseAbi().contract;
  const reader: StateReader = new StateReader(
    StateReaderHelper.readStateFile("token_contract.state"),
    contract
  );

  const state: ScValueStruct = reader.readState();

  expect(requireNonNull(state.getFieldValue("name")).stringValue()).toEqual("Token Name");
  expect(requireNonNull(state.getFieldValue("decimals")).asNumber()).toEqual(18);
  expect(requireNonNull(state.getFieldValue("symbol")).stringValue()).toEqual("SYMB");

  const expectedAddress = bytesFromHex("020000000000000000000000000000000000000001");
  const address: ScValueAddress = requireNonNull(state.getFieldValue("owner")).addressValue();
  expect(address.value).toEqual(expectedAddress);

  expect(
    requireNonNull(state.getFieldValue("total_supply")).asBN().eq(new BN("123123123"))
  ).toBeTruthy();

  const balancesMap: ScValueMap = requireNonNull(state.getFieldValue("balances")).mapValue();
  expect(
    requireNonNull(balancesMap.get(address)) // commontests-ignore-array
      .asBN()
      .eq(new BN("123123123"))
  ).toBeTruthy();

  expect(requireNonNull(state.getFieldValue("allowed")).mapValue().isEmpty()).toBeTruthy();
});

test("testTokenContractAfterAction", () => {
  expect(
    StateReaderHelper.stringify("token_contract.abi", "token_contract_transfer.state")
  ).toEqual(
    "#[state]\n" +
      "TokenContractState {\n" +
      '    name: "Token Name".to_string(),\n' +
      "    decimals: 18u8,\n" +
      '    symbol: "SYMB".to_string(),\n' +
      '    owner: "020000000000000000000000000000000000000001",\n' +
      "    total_supply: 123123123u64,\n" +
      "    balances: BTreeMap::from([\n" +
      '        ("020000000000000000000000000000000000000001", 123123081u64),\n' +
      '        ("020000000000000000000000000000000000000002", 42u64)\n' +
      "    ]),\n" +
      "    allowed: BTreeMap::from([\n" +
      "    ]),\n" +
      "}"
  );
});

test("initialAuctionState", () => {
  expect(
    StateReaderHelper.stringify("rust_example_auction_contract.abi", "auction_initial.state")
  ).toEqual(
    "#[state]\n" +
      "TokenContractState {\n" +
      '    contract_owner: "010000000000000000000000000000000000000001",\n' +
      "    start_time: 12346i64,\n" +
      "    end_time: 12423i64,\n" +
      "    token_amount_for_sale: 100u64,\n" +
      '    commodity_token_type: "02eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",\n' +
      '    currency_token_type: "02dddddddddddddddddddddddddddddddddddddddd",\n' +
      "    highest_bidder: Bid {\n" +
      '        bidder: "010000000000000000000000000000000000000001",\n' +
      "        amount: 0u64\n" +
      "    },\n" +
      "    reserve_price: 20u64,\n" +
      "    min_increment: 5u64,\n" +
      "    claim_map: BTreeMap::from([\n" +
      "    ]),\n" +
      "    status: 0u8,\n" +
      "}"
  );
});

test("optionVecState", () => {
  expect(StateReaderHelper.stringify("contract_option_vec_v3.abi", "option_vec.state")).toEqual(
    "#[state]\n" +
      "ExampleContractState {\n" +
      "    option_vec: Some(vec![\n" +
      "        1u64,\n" +
      "        2u64\n" +
      "    ]),\n" +
      "    option_option: None,\n" +
      "}"
  );
});

test("contractSetState", () => {
  // Two elements, BTreeSet::from([1u64, 2u64])
  const bytes = bytesFromHex("0200000001000000000000000200000000000000");
  expect(StateReaderHelper.stringifyBytes("contract_set.abi", bytes)).toEqual(
    "#[state]\n" +
      "ExampleContractState {\n" +
      "    my_set: BTreeSet::from([\n" +
      "        1u64,\n" +
      "        2u64\n" +
      "    ]),\n" +
      "}"
  );
});

test("contractArrays", () => {
  // First array: 0-15, second: 16-20
  const bytes = bytesFromHex("000102030405060708090a0b0c0d0e0f" + "101112131415");
  expect(StateReaderHelper.stringifyBytes("contract_byte_arrays_v3.abi", bytes)).toEqual(
    "#[state]\n" +
      "ExampleContractState {\n" +
      "    my_array: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],\n" +
      "    my_array_2: [16, 17, 18, 19, 20],\n" +
      "}"
  );
});

test("contractBooleans", () => {
  const initBytes = bytesFromHex("01");
  expect(StateReaderHelper.stringifyBytes("contract_booleans_v3.abi", initBytes)).toEqual(
    "#[state]\n" + "ExampleContractState {\n" + "    my_bool: true,\n" + "}"
  );

  const changedStateByte = bytesFromHex("00");
  expect(StateReaderHelper.stringifyBytes("contract_booleans_v3.abi", changedStateByte)).toEqual(
    "#[state]\n" + "ExampleContractState {\n" + "    my_bool: false,\n" + "}"
  );
});

test("test128BitContract", () => {
  const bytes = bytesFromHex(
    // 1339673755198158348465859924523878152
    "08070605040302010807060504030201" +
      // -170141183460469231731687303715884105728
      "00000000000000000000000000000080"
  );

  expect(StateReaderHelper.stringifyBytes("contract_u128_and_i128_v3.abi", bytes)).toEqual(
    "#[state]\n" +
      "ExampleContractState {\n" +
      "    my_u128: 1339673755198158348465859924523878152u128,\n" +
      "    my_i128: -170141183460469231731687303715884105728i128,\n" +
      "}"
  );
});

test("testNumbers", () => {
  expect(
    StateReaderHelper.stringify("contract_numbers_v4.abi", "contract_numbers_simple.state")
  ).toEqual(
    "#[state]\n" +
      "ExampleContractState {\n" +
      "    my_u8: 1u8,\n" +
      "    my_u16: 2u16,\n" +
      "    my_u32: 3u32,\n" +
      "    my_u64: 4u64,\n" +
      "    my_i8: -1i8,\n" +
      "    my_i16: -2i16,\n" +
      "    my_i32: -3i32,\n" +
      "    my_i64: -4i64,\n" +
      "}"
  );
});
