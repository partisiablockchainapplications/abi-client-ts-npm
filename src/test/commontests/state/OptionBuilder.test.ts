/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("contractOption", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-options
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile("contract_options_v3.abi");
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  stateProducer.addOption().addU64(1);
  stateProducer.addOption().addString("option");
  stateProducer.addOption().addAddress(bytesFromHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"));
  stateProducer.addOption().addBool(false);
  stateProducer.addOption();
  const state = BuilderHelper.builderToBytesLe(stateProducer);
  const expected = concatBytes(
    Buffer.from([0x01]),
    bytesFromHex("0100000000000000"),
    Buffer.from([0x01]),
    bytesFromStringLe("option"),
    Buffer.from([0x01]),
    bytesFromHex("b002131a2b3c6741b42cfa4c33a2830602a3f2e9ff"),
    Buffer.from([0x01]),
    Buffer.from([0x00]),
    Buffer.from([0x00])
  );
  expect(state).toEqual(expected);

  // Test missing element
  const producerNoOption: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contractAbi,
    ""
  );
  producerNoOption.addOption();
  expect(() => BuilderHelper.builderToBytesLe(producerNoOption)).toThrowError(
    "Missing argument 'option_string'"
  );

  // Test without type checking
  const producerNoTypeCheck: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contractAbi,
    "",
    null
  );
  producerNoTypeCheck.addOption().addString("hello");
  const stateNoTypeCheck = BuilderHelper.builderToBytesLe(producerNoTypeCheck);
  const expectedNoTypeCheck = concatBytes(Buffer.from([0x01]), bytesFromStringLe("hello"));
  expect(stateNoTypeCheck).toEqual(expectedNoTypeCheck);

  // Test without type checking and without option
  const producerNoTypeCheckNoOption: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contractAbi,
    "",
    null
  );
  producerNoTypeCheckNoOption.addOption();
  const stateNoTypeCheckNoOption = BuilderHelper.builderToBytesLe(producerNoTypeCheckNoOption);
  expect(stateNoTypeCheckNoOption).toEqual(Buffer.from([0x00]));
});

test("noTypeCheckState", () => {
  const spec = TestingHelper.sizedByteArrayTypeSpec(4);
  const structAbi = new StructTypeSpec("name", list(TestingHelper.fieldAbi("f", spec)));
  const contractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contractAbi,
    "",
    null
  );
  stateProducer.addOption().addU8(0x0a);
  stateProducer.addOption();
  const state = BuilderHelper.builderToBytesLe(stateProducer);
  const expected = bytesFromHex("010A00");
  expect(state).toEqual(expected);

  const producerWithTwoOptions: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contractAbi,
    "",
    null
  );
  producerWithTwoOptions.addOption().addOption().addI8(-1);
  const stateTwoOptions = BuilderHelper.builderToBytesLe(producerWithTwoOptions);
  const expectedTwoOptions = bytesFromHex("0101ff");
  expect(stateTwoOptions).toEqual(expectedTwoOptions);
});

test("doubleAdd", () => {
  const structAbi = new StructTypeSpec(
    "name",
    list(
      TestingHelper.fieldAbi(
        "f",
        TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u32))
      )
    )
  );
  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));
  const stateProducer = new StructProducer<LittleEndianByteOutput>(simpleContractAbi, "");
  expect(() => stateProducer.addOption().addU32(1).addU32(1)).toThrowError(
    "Cannot set option value twice."
  );
});

test("invalidType", () => {
  const structAbi = new StructTypeSpec(
    "name",
    list(TestingHelper.fieldAbi("f", TestingHelper.simpleTypeSpec(TypeIndex.u64)))
  );

  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));
  const stateProducer = new StructProducer<LittleEndianByteOutput>(simpleContractAbi, "");

  expect(() => BuilderHelper.builderToBytesLe(stateProducer.addBool(true))).toThrowError(
    "In /f, Expected type u64, but got bool"
  );
});

test("typeCheck", () => {
  const structAbi = new StructTypeSpec(
    "name",
    list(
      TestingHelper.fieldAbi(
        "f",
        TestingHelper.optionTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64))
      )
    )
  );

  const simpleContractAbi = new ContractAbi([structAbi], [], TestingHelper.namedTypeRef(0));

  expect(() =>
    BuilderHelper.builderToBytesLe(
      new StructProducer<LittleEndianByteOutput>(simpleContractAbi, "").addOption().addBool(true)
    )
  ).toThrowError("In /f, Expected type u64, but got bool");

  expect(() =>
    BuilderHelper.builderToBytesLe(
      new StructProducer<LittleEndianByteOutput>(simpleContractAbi, "").addOption().addOption()
    )
  ).toThrowError("In /f, Expected type u64, but got Option");
});
