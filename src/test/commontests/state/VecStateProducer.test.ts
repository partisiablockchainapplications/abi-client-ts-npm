/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
// This file is auto-generated from Java to ensure TS and Java API's are identical
// See https://gitlab.com/secata/pbc/language/abi-client/.
/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  bytesFromHex,
  bytesFromStringBe,
  bytesFromStringLe,
  castNamedTypeRef,
  concatBytes,
  list,
  requireNonNull,
  TestingHelper,
} from "../TestingHelper";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  FileAbi,
  FieldAbi,
  FnAbi,
  FnKinds,
  FnRpcBuilder,
  FunctionFormat,
  MapTypeSpec,
  OptionTypeSpec,
  ScValue,
  ScValueAddress,
  ScValueMap,
  ScValueNumber,
  ScValueOption,
  ScValueSet,
  ScValueSizedByteArray,
  ScValueString,
  ScValueStruct,
  ScValueVector,
  ScValueEnum,
  SetTypeSpec,
  ShortnameType,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  StateReader,
  NamedTypeRef,
  NamedTypeSpec,
  NamedTypesFormat,
  StructTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
  MapProducer,
  StructProducer,
  EnumVariantProducer,
  VecProducer,
  AbstractBuilder,
  RpcReader,
  JsonRpcConverter,
  ScValueBool,
  JsonValueConverter,
  HashMap,
  EnumTypeSpec,
  EnumVariant,
} from "../../../main";
import { BuilderHelper } from "../BuilderHelper";
import { StateReaderHelper } from "../StateReaderHelper";
import { RpcReaderHelper } from "../RpcReaderHelper";
import { ParserHelper } from "../ParserHelper";
import { ValueHelper } from "../ValueHelper";
import {
  BigEndianByteOutput,
  LittleEndianByteInput,
  LittleEndianByteOutput,
  BigEndianByteInput,
} from "bitmanipulation-ts";
import BN from "bn.js";
/* eslint-enable @typescript-eslint/no-unused-vars */

test("getAndSize", () => {
  const spec = TestingHelper.vecTypeSpec(TestingHelper.simpleTypeSpec(TypeIndex.u64));
  const producer = new VecProducer<BigEndianByteOutput>(spec, null, "");

  expect(producer.size()).toEqual(0); // commontests-ignore-array

  producer.addU64(44);
  expect(producer.size()).toEqual(1); // commontests-ignore-array

  producer.addU64(45);
  expect(producer.size()).toEqual(2); // commontests-ignore-array

  const bytes0 = BuilderHelper.builderToBytesBe(producer.get(0)); // commontests-ignore-array
  const bytes1 = BuilderHelper.builderToBytesBe(producer.get(1)); // commontests-ignore-array
  expect(bytes0).toEqual(Buffer.from([0, 0, 0, 0, 0, 0, 0, 44]));
  expect(bytes1).toEqual(Buffer.from([0, 0, 0, 0, 0, 0, 0, 45]));
});

test("assertTypeErrorInStruct", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-struct
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_simple_struct_v3.abi"
  );
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  expect(() =>
    stateProducer.addU64(1).addStruct().addU64(2).addVec().addString("valueType")
  ).toThrowError("In /my_struct/some_vector/0, Expected type u64, but got String");

  const noVecBuilder: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  expect(() => noVecBuilder.addU64(1).addVec()).toThrowError(
    "In /my_struct, Expected type Named, but got Vec"
  );
});

test("testVec", () => {
  // Contract source can be found at:
  // https://gitlab.com/privacyblockchain/language/rust-example-testing-contract/-/tree/main/contract-simple-vector
  const contractAbi: ContractAbi = TestingHelper.getContractAbiFromFile(
    "contract_simple_vector_v3.abi"
  );
  const stateProducer: StructProducer<LittleEndianByteOutput> = new StructProducer(contractAbi, "");
  stateProducer.addVec().addU64(1).addU64(2).addU64(3);
  const state = BuilderHelper.builderToBytesLe(stateProducer);
  const expected = concatBytes(
    bytesFromHex("03000000"),
    bytesFromHex("0100000000000000"),
    bytesFromHex("0200000000000000"),
    bytesFromHex("0300000000000000")
  );
  expect(state).toEqual(expected);

  // Testing with no type checking
  const noTypeCheckBuilder: StructProducer<LittleEndianByteOutput> = new StructProducer(
    contractAbi,
    "",
    null
  );
  noTypeCheckBuilder.addVec().addU64(1).addU64(2).addU64(3);
  expect(BuilderHelper.builderToBytesLe(noTypeCheckBuilder)).toEqual(expected);
});
