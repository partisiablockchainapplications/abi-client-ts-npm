/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { readFileSync } from "fs";
import {
  AbiParser,
  AbiVersion,
  ArgumentAbi,
  Configuration,
  ContractAbi,
  EnumVariant,
  FieldAbi,
  FileAbi,
  FnAbi,
  FnKind,
  FnRpcBuilder,
  MapTypeSpec,
  NamedTypeRef,
  OptionTypeSpec,
  SetTypeSpec,
  SimpleTypeIndex,
  SimpleTypeSpec,
  SizedByteArrayTypeSpec,
  TypeIndex,
  TypeSpec,
  VecTypeSpec,
} from "../../main";
import { BigEndianByteOutput, LittleEndianByteOutput } from "bitmanipulation-ts";

export const TestingHelper = {
  readBinaryFile(fileName: string): Buffer {
    return readFileSync("src/test-resources/abi-contracts/" + fileName);
  },

  loadAbiParserFromFile(inputFile: string) {
    return new AbiParser(this.readBinaryFile(inputFile));
  },

  abiParserFromBytes(bytes: Buffer): AbiParser {
    return new AbiParser(bytes);
  },

  createBuilderFromFile(filename: string, actionName: string): FnRpcBuilder {
    return new FnRpcBuilder(actionName, this.getContractAbiFromFile(filename));
  },

  getContractAbiFromFile(filename: string) {
    return this.loadAbiParserFromFile(filename).parseAbi().contract;
  },

  simpleTypeSpec(typeIndex: SimpleTypeIndex): SimpleTypeSpec {
    return { typeIndex };
  },

  mapTypeSpec(keyType: TypeSpec, valueType: TypeSpec): MapTypeSpec {
    return { typeIndex: TypeIndex.Map, keyType, valueType };
  },

  vecTypeSpec(valueType: TypeSpec): VecTypeSpec {
    return { typeIndex: TypeIndex.Vec, valueType };
  },

  setTypeSpec(valueType: TypeSpec): SetTypeSpec {
    return { typeIndex: TypeIndex.Set, valueType };
  },

  optionTypeSpec(valueType: TypeSpec): OptionTypeSpec {
    return { typeIndex: TypeIndex.Option, valueType };
  },

  sizedByteArrayTypeSpec(length: number): SizedByteArrayTypeSpec {
    return { typeIndex: TypeIndex.SizedByteArray, length };
  },

  namedTypeRef(index: number): NamedTypeRef {
    return { typeIndex: TypeIndex.Named, index };
  },

  enumVariant(discriminant: number, def: NamedTypeRef): EnumVariant {
    return { discriminant, def };
  },

  fieldAbi(name: string, type: TypeSpec): FieldAbi {
    return { name, type };
  },

  argumentAbi(name: string, type: TypeSpec): ArgumentAbi {
    return { name, type };
  },

  fnAbi(kind: FnKind, name: string, shortname: Buffer, args: ArgumentAbi[]): FnAbi {
    return { kind, name, shortname, arguments: args };
  },

  fileAbi(
    header: string,
    versionBinder: AbiVersion,
    versionClient: AbiVersion,
    format: Configuration,
    contract: ContractAbi
  ): FileAbi {
    return { header, versionBinder, versionClient, format, contract };
  },
};

export function requireNonNull<T>(obj: T | undefined | null): T {
  if (obj === undefined || obj === null) {
    throw new Error("Null pointer / undefined exception");
  }
  return obj;
}

export function castNamedTypeRef(obj: TypeSpec): NamedTypeRef {
  return obj as NamedTypeRef;
}

export function bytesFromHex(hex: string): Buffer {
  return Buffer.from(hex, "hex");
}

export function bytesFromStringBe(string: string) {
  const writer = new BigEndianByteOutput();
  writer.writeString(string);
  return writer.toBuffer();
}

export function bytesFromStringLe(string: string) {
  const writer = new LittleEndianByteOutput();
  writer.writeString(string);
  return writer.toBuffer();
}

export function concatBytes(...bytes: Buffer[]): Buffer {
  return Buffer.concat(bytes);
}

export function list<T>(...elements: T[]): T[] {
  return elements;
}
