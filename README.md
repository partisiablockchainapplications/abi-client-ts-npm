# The TypeScript ABI client

## Setup

Requires: `tsc`, `npm`

Install dependencies with: `npm i`

To build: `npm run build`

To test : `npm run test`